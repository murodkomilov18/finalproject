package dev.komur.daos;

import dev.komur.entities.Food;
import dev.komur.mapper.FoodMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodDAO extends DAO<Food, String> {
    private static volatile FoodDAO instance;
    private final FoodMapper mapper = FoodMapper.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final String INSERT_FOOD_QUERY = """
            insert into food(name, price, category_id, created_at, created_by) values(?, ?, ?, ?, ?) returning id;
            """;

    private static final String UPDATE_FOOD_QUERY = """
            update food set name= ?, price = ?, category_id = ?, updated_at = ?, updated_by = ? where id = ?;
            """;

    public static FoodDAO getInstance() {
        if (instance == null) {
            synchronized (FoodDAO.class){
                if (instance == null){
                    instance = new FoodDAO();
                }
            }
        }
        return instance;
    }
    @Override
    public Food save(Food food) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_FOOD_QUERY)) {
            pr.setString(1, food.getName());
            pr.setDouble(2, food.getPrice());
            pr.setInt(3, Integer.parseInt(food.getCategoryId()));
            pr.setDate(4, Date.valueOf(food.getCreatedAt().toLocalDate()));
            pr.setString(5, food.getCreatedBy());

            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                food.setId(String.valueOf(resultSet.getInt("id")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving the food in database");
        }
        return food;
    }

    @Override
    public Optional<Food> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from food where id = ?;")) {
            pr.setInt(1 , Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Food food = mapper.fromResultSet(resultSet);
                return Optional.of(food);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the food from database");
        }
        return Optional.empty();
    }

    @Override
    public List<Food> getAll() {
        Connection connection = getConnection();
        List<Food> items;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from food where is_deleted = false;");
            items = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all food from database");
        }
        return items;
    }

    @Override
    public int update(Food food) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_FOOD_QUERY)){
            pr.setString(1, food.getName());
            pr.setDouble(2, food.getPrice());
            pr.setInt(3, Integer.parseInt(food.getCategoryId()));
            pr.setDate(4, Date.valueOf(LocalDate.now()));
            pr.setString(5, food.getUpdatedBy());
            pr.setInt(6, Integer.parseInt(food.getId()));
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the food");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update food set is_deleted = true where id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            return pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the food");
        }
    }

    public List<Food> getByPage(int recordsPerPage, int offset) {
        Connection connection = getConnection();
        List<Food> items;
        try(PreparedStatement pr = connection.prepareStatement("select * from food where is_deleted = false order by name limit ? offset ?;")) {
            pr.setInt(1, recordsPerPage);
            pr.setInt(2, offset);
            ResultSet resultSet = pr.executeQuery();
            items = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting foods by page from database");
        }
        return items;
    }

    public List<Food> getByCategory(String categoryId) {
        Connection connection = getConnection();
        List<Food> items;
        try(PreparedStatement pr = connection.prepareStatement("select * from food where is_deleted = false and category_id = ?;")) {
            pr.setInt(1, Integer.parseInt(categoryId));
            ResultSet resultSet = pr.executeQuery();
            items = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting foods by category from database");
        }
        return items;
    }
}
