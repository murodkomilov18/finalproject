<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 23-07-2023
  Time: 09:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Payment Page</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
    <div class="app-container modal">
        <div class="basket-row">
            <div class="basket-header"><a href="/user"><span class="close">&times;</span></a></div>
            <div class="wrapper_1">
                <span class="span">How would you like to pay for the order?</span>
            </div>
            <div class="form">
                <a class="app-content-headerButton" href="/pay?id=${id}&total=${total}">By account</a>
                <a class="app-content-headerButton" href="/pay?id=${id}">By cash</a>
            </div>
        </div>
    </div>
</body>
</html>
