package dev.komur.servlets.admin.category;

import dev.komur.dtos.food.CategoryCreateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.category.CategoryServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "AddCategoryServlet", value = "/add_category")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)public class AddCategoryServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("pageHeader", "Add Category");
        request.getRequestDispatcher("/admin/category/add_category.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();

        final String name = request.getParameter("f_name");

        if (Objects.isNull(name) || name.isBlank())
            throw new CustomizedException("Category name is required", 400);
        if (request.getPart("f_image").getSize() <= 0)
            throw new CustomizedException("Image is required", 400);

        final Part filePart = request.getPart("f_image");
        CategoryCreateDTO dto = new CategoryCreateDTO(name, userId, filePart.getSubmittedFileName(), filePart.getSize(), filePart.getInputStream().readAllBytes());
        Response result = CategoryServiceImpl.getInstance().create(dto);
        request.setAttribute("message", result.getMessage());
        request.setAttribute("next_link", request.getSession().getAttribute("previous_link"));
        request.getRequestDispatcher("/payload/success.jsp").forward(request, response);
    }
}
