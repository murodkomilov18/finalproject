package dev.komur.enums;

public enum UserStatus {
        ACTIVE(0),
        NOT_ACTIVE(-1),
        BLOCKED(-50),
        BANNED(-100);
        private final int level;

        UserStatus(int level) {
            this.level = level;
        }

        public int getLevel() {
            return level;
        }
    }