package dev.komur.servlets.user.order;

import dev.komur.dtos.order.OrderDTO;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Objects;

@WebServlet(name = "BasketServlet", value = "/basket")
public class BasketServlet extends HttpServlet {
    private final BasketService service = BasketServiceImpl.getInstance();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id != null) service.deleteOrderDetail(id);
        String userId = request.getSession().getAttribute("userId").toString();
        OrderDTO dto = service.getOrderDetailsByUserId(userId);
        if (dto.getOrderDetails().isEmpty()){
            response.sendRedirect("/user");
        }else {
            request.setAttribute("details", dto.getOrderDetails());
            request.setAttribute("total", dto.getTotal());
            request.getRequestDispatcher("/user/basket.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        String time = request.getParameter("time");
        String total = request.getParameter("total");
        if (Objects.isNull(time) || time.isBlank()) throw new CustomizedException("Time should be selected", 400);
        LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm"));
        if (localTime.isBefore(LocalTime.now())) throw new CustomizedException("Time should be after from now", 400);
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), localTime);
        String id = service.setOrderTime(userId, dateTime);
        request.setAttribute("total", total);
        request.setAttribute("id", id);
        request.getRequestDispatcher("/user/payment.jsp").forward(request, response);
    }
}
