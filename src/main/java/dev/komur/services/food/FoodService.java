package dev.komur.services.food;

import dev.komur.dtos.food.FoodCreateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.entities.Food;
import dev.komur.services.base.GenericCrudService;

import java.util.List;

public interface FoodService extends GenericCrudService<FoodCreateDTO, FoodUpdateDTO, String> {
    List<FoodUpdateDTO> getFoodsByPage(int recordsPerPage, int offset);
    List<FoodUpdateDTO> getFoodsByCategory(String categoryId);
    FoodUpdateDTO getWithImage(String id);
    int getNumberOfAllFoods();
}
