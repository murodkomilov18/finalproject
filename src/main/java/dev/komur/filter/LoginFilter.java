package dev.komur.filter;

import dev.komur.daos.UserDAO;
import dev.komur.dtos.user.LoginRequest;
import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.exceptions.CustomizedException;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.Objects;

@WebFilter(filterName = "LoginFilter", urlPatterns = "/auth/login")
public class LoginFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        final String email = req.getParameter("email");
        final String password = req.getParameter("password");

        if (req.getMethod().equalsIgnoreCase("post")){
            if (Objects.isNull(email) || email.isBlank() ||
                !email.matches("^[\\w!#$%&amp;'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&amp;'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"))
                throw new CustomizedException("Email is not valid", 400);
            if (!UserDAO.getInstance().existByEmail(email))
                throw new CustomizedException("Provided email is not registered in the system", 404);
            if (Objects.isNull(password) || password.isBlank() || password.contains(" "))
                throw new CustomizedException("Password is not valid", 400);

            req.setAttribute("loginDto", new LoginRequest(email, password));
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
