package dev.komur.dtos.order;

public record OrderDetailDTO(String id, String image, String name, Double price, Integer quantity) {
}
