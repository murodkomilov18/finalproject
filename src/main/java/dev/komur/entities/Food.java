package dev.komur.entities;

import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Food extends BaseEntity {
    private String name;
    private Double price;
    private String categoryId;

    @Builder(builderMethodName = "childBuilder")
    public Food(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, @NonNull String name, @NonNull Double price, @NonNull String categoryId) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
    }
}
