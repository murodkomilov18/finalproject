package dev.komur.exceptions;

public class BeanNotFoundException extends RuntimeException{
    private final Integer code;

    public BeanNotFoundException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
