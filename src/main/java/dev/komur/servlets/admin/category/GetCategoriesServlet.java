package dev.komur.servlets.admin.category;

import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.services.category.CategoryService;
import dev.komur.services.category.CategoryServiceImpl;
import dev.komur.services.food.FoodService;
import dev.komur.services.food.FoodServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "GetCategoriesServlet", value = "/category")
public class GetCategoriesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int recordsPerPage = 5;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));

        CategoryService service = CategoryServiceImpl.getInstance();
        List<CategoryUpdateDTO> categories = service.getCategoriesByPage(recordsPerPage, (page - 1) * recordsPerPage);
        int noOfRecords = service.getNumberOfAllFoods();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        request.setAttribute("categories", categories);
        request.getSession().setAttribute("previous_link", request.getRequestURI());
        request.getRequestDispatcher("/admin/category/categories.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
