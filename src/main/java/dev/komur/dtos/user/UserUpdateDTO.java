package dev.komur.dtos.user;

import dev.komur.dtos.GenericDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UserUpdateDTO extends GenericDto {
    private String firstName;
    private String lastName;
    private String email;

    @Builder(builderMethodName = "childBuilder")
    public UserUpdateDTO(String id, String firstName, String lastName, String email) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}
