package dev.komur.daos;

import dev.komur.entities.User;
import dev.komur.enums.UserStatus;
import dev.komur.exceptions.CustomizedException;
import dev.komur.mapper.UserMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDAO extends DAO<User, String>{

    private static volatile UserDAO instance;
    private final UserMapper mapper = UserMapper.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());

    private static final String INSERT_USER_QUERY = """
            insert into users(id, firstname, lastname, email, password, status, role, code, created_at) values(?, ?, ?, ?, ?, ?, ?, ?, ?);
            """;

    private static final String UPDATE_USER_QUERY = """
            update users set firstname= ?, lastname = ?, email = ?, status = ?, updated_at = ?, loyalty_points = ?, account = ? where id = ?;
            """;

    public static UserDAO getInstance() {
        if (instance == null) {
            synchronized (UserDAO.class){
                if (instance == null){
                    instance = new UserDAO();
                }
            }
        }
        return instance;
    }

    public boolean existByEmail(String email) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from users where email = ?;")) {
            pr.setString(1 ,email);
            ResultSet resultSet = pr.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while finding user by email from database");
        }
    }

    @Override
    public Optional<User> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from users where id = ?;")) {
            pr.setString(1 ,id);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                User user = mapper.fromResultSet(resultSet);
                return Optional.of(user);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the user from database");
        }
        return Optional.empty();
    }

    public Optional<User> getByEmail(String email){
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from users where email = ?;")) {
            pr.setString(1 ,email);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                User user = mapper.fromResultSet(resultSet);
                return Optional.of(user);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the user from database");
        }
        return Optional.empty();
    }

    public void updatePassword(String password, String userId){
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("update users set password = ? where id = ?;")){
            pr.setString(1, password);
            pr.setString(2, userId);
            pr.execute();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the user");
        }
    }
    @Override
    public List<User> getAll() {
        Connection connection = getConnection();
        List<User> users;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from users where role = 'CLIENT' and is_deleted = false;");
            users = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all users from database");
        }
        return users;
    }

    @Override
    public User save(User user) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_USER_QUERY)) {
            pr.setString(1, user.getId());
            pr.setString(2, user.getFirstName());
            pr.setString(3, user.getLastName());
            pr.setString(4, user.getEmail());
            pr.setString(5, user.getPassword());
            pr.setString(6, user.getStatus().name());
            pr.setString(7, user.getRole().name());
            pr.setString(8, user.getCode());
            pr.setDate(9, Date.valueOf(user.getCreatedAt().toLocalDate()));

            pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while creating user in database");
        }
        return user;
    }

    @Override
    public int update(User user) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_USER_QUERY)){
            pr.setString(1, user.getFirstName());
            pr.setString(2, user.getLastName());
            pr.setString(3, user.getEmail());
            pr.setString(4, user.getStatus().name());
            pr.setDate(5, Date.valueOf(LocalDate.now()));
            pr.setLong(6, user.getLoyaltyPoints());
            pr.setDouble(7, user.getAccount());
            pr.setString(8, user.getId());
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the user");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update users set is_deleted = true where id = ?;")){
            pr.setString(1, id);
            return pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the user");
        }
    }

    public void changeUserStatus(String userId, UserStatus status) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update users set status = ? where id = ?;")){
            pr.setString(1, status.name());
            pr.setString(2, userId);
            pr.executeUpdate();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while changing the user status");
        }
    }
}
