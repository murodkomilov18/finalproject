package dev.komur.dtos.food;

import dev.komur.dtos.Dto;

public record CategoryCreateDTO(String name, String createdBy, String fileOriginalName, Long size, byte[] mainContent) implements Dto {
}
