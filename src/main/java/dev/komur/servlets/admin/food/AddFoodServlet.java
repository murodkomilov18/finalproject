package dev.komur.servlets.admin.food;

import dev.komur.dtos.food.FoodCreateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.category.CategoryServiceImpl;
import dev.komur.services.food.FoodServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "AddFoodServlet", value = "/add_food")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class AddFoodServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Response all = CategoryServiceImpl.getInstance().getAll();
        request.setAttribute("categories", all.getData());
        request.setAttribute("pageHeader", "Add Food to Menu");
        request.getRequestDispatcher("/admin/food/add_food.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();

        final String name = request.getParameter("f_name");
        final double price;
        try {
             price = Double.parseDouble(request.getParameter("f_price"));
        }catch (Exception e){
            throw new CustomizedException("Item price input invalid", 400);
        }
        final String categoryId = request.getParameter("category_id");

        if (Objects.isNull(name) || name.isBlank())
            throw new CustomizedException("Item name is required", 400);
        if (Objects.isNull(categoryId) || categoryId.isBlank())
            throw new CustomizedException("Category should be selected", 400);
        if (price <= 0)
            throw new CustomizedException("Item price should be greater 0", 400);
        if (request.getPart("f_image").getSize() <= 0)
            throw new CustomizedException("Image is required", 400);
        final Part filePart = request.getPart("f_image");

        FoodCreateDTO dto = new FoodCreateDTO(name, price, categoryId,
                userId, filePart.getSubmittedFileName(), filePart.getSize(),
                filePart.getInputStream().readAllBytes());
        Response result = FoodServiceImpl.getInstance().create(dto);
        request.setAttribute("message", result.getMessage());
        request.setAttribute("next_link", request.getSession().getAttribute("previous_link"));
        request.getRequestDispatcher("/payload/success.jsp").forward(request, response);
    }
}
