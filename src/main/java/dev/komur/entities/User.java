package dev.komur.entities;

import dev.komur.enums.UserStatus;
import dev.komur.enums.UserRole;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User extends BaseEntity {
    @NonNull
    private String firstName;
    private String lastName;
    @NonNull
    private String email;
    @NonNull
    private String password;
    @NonNull
    private String code;
    private long loyaltyPoints;
    private double account;
    private UserStatus status = UserStatus.NOT_ACTIVE;
    private UserRole role = UserRole.CLIENT;

    @Builder(builderMethodName = "childBuilder")
    public User(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt,
                String createdBy, String updatedBy, @NonNull String firstName, String lastName,
                @NonNull String password, UserStatus status, @NonNull String email, UserRole role,
                @NonNull String code, long loyaltyPoints, double account) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.role = role;
        this.code = code;
        this.loyaltyPoints = loyaltyPoints;
        this.account = account;
    }
}
