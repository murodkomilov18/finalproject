package dev.komur.dtos.food;

import dev.komur.dtos.GenericDto;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FoodUpdateDTO extends GenericDto {
    private String name;
    private Double price;
    private String fileOriginalName;
    private Long size;
    private byte[] mainContent;
    private LocalDateTime updatedAt;
    private String updatedBy;
    private String categoryId;

    @Builder(builderMethodName = "childBuilder")
    public FoodUpdateDTO(String id, String name, Double price, String fileOriginalName, Long size, byte[] mainContent, LocalDateTime updatedAt, String updatedBy, String categoryId) {
        super(id);
        this.name = name;
        this.price = price;
        this.fileOriginalName = fileOriginalName;
        this.size = size;
        this.mainContent = mainContent;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
        this.categoryId = categoryId;
    }
}
