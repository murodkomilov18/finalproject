package dev.komur.mapper;

import dev.komur.dtos.attachment.AttachmentDTO;
import dev.komur.entities.Attachment;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AttachmentMapper implements BaseMapper<Attachment, ResultSet, AttachmentDTO> {
    private final Logger logger = Logger.getLogger(getClass().getName());

    private static volatile AttachmentMapper mapper;

    public static AttachmentMapper getInstance() {
        if (mapper == null) {
            synchronized (AttachmentMapper.class){
                if (mapper == null){
                    mapper = new AttachmentMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public Attachment fromCreateDTO(@NonNull AttachmentDTO dto) {
        return Attachment.childBuilder()
                .fileOriginalNam(dto.getFileOriginalName())
                .size(dto.getSize())
                .mainContent(dto.getMainContent())
                .linkedId(dto.getLinkedId())
                .linkedName(dto.getLinkedName())
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public Attachment fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return Attachment.childBuilder()
                    .id(resultSet.getString("id"))
                    .fileOriginalNam(resultSet.getString("file_original_name"))
                    .size(resultSet.getLong("size"))
                    .mainContent(resultSet.getBytes("main_content"))
                    .linkedId(resultSet.getString("linked_id"))
                    .linkedName(resultSet.getString("linked_name"))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<Attachment> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<Attachment> attachments = new ArrayList<>();
        try {
            while (resultSet.next()){
                attachments.add(Attachment.childBuilder()
                        .id(resultSet.getString("id"))
                        .fileOriginalNam(resultSet.getString("file_original_name"))
                        .size(resultSet.getLong("size"))
                        .mainContent(resultSet.getBytes("main_content"))
                        .linkedId(resultSet.getString("linked_id"))
                        .linkedName(resultSet.getString("linked_name"))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return attachments;
    }
}
