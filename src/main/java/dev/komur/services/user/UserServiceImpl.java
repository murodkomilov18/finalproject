package dev.komur.services.user;

import dev.komur.config.AppContext;
import dev.komur.daos.AttachmentDAO;
import dev.komur.daos.UserDAO;
import dev.komur.dtos.response.Response;
import dev.komur.dtos.user.ChangePasswordRequest;
import dev.komur.dtos.user.LoginRequest;
import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.dtos.user.UserUpdateDTO;
import dev.komur.entities.Attachment;
import dev.komur.entities.User;
import dev.komur.enums.UserStatus;
import dev.komur.exceptions.CustomizedException;
import dev.komur.exceptions.UserNotFoundException;
import dev.komur.mapper.UserMapper;
import dev.komur.services.base.AbstractService;
import dev.komur.services.mail.MailService;
import dev.komur.utils.BaseUtil;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.Part;
import lombok.NonNull;
import lombok.SneakyThrows;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Logger;

public class UserServiceImpl extends AbstractService<UserDAO, UserMapper> implements UserService {

    private static volatile UserService service;
    private final Logger logger = Logger.getLogger(getClass().getName());

    public UserServiceImpl(UserDAO dao, UserMapper mapper) {
        super(dao, mapper);
    }

    public static UserService getInstance() {
        if (service == null) {
            synchronized (UserServiceImpl.class){
                if (service == null){
                    service = new UserServiceImpl(
                            AppContext.getBean(UserDAO.class),
                            AppContext.getBean(UserMapper.class)
                    );
                }
            }
        }
        return service;
    }

    @Override
    public Response create(@NonNull UserCreateDTO dto) {
        try {
            User user = mapper.fromCreateDTO(dto);
            user.setPassword(util.encode(user.getPassword()));
            user.setId(BaseUtil.generateUUID());
            user.setCode(String.valueOf(new Random().nextInt(9999, 100000)));
            String content = ("<div style=\"display:flex;justify-content:center;\">" +
                    "<a href=\"http://localhost:8080/auth/check?userId=%s&code=%s\" " +
                    "style=\"background-color:lime;padding:20px 30px;text-decoration:none;font-weight:bold\">" +
                    "Activate your account</a></div>").formatted(user.getId(), user.getCode());
            Map<String, String> body = Map.of(
                    "subject", "Confirmation Code",
                    "content", content,
                    "email", user.getEmail()
            );
            MailService.getInstance().sendEmail(body);
            dao.save(user);
            return new Response(user.getId(), "We've sent activation link to your email. Please activate your account");
        } catch (Exception e) {
            logger.severe(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Response update(@NonNull UserUpdateDTO dto) {
        BaseUtil.checkParam(dto.getId(), "Id");
        Optional<User> userOptional = dao.getById(dto.getId());
        if (userOptional.isEmpty())
            throw new CustomizedException("User not found by id :%s".formatted(dto.getId()), 404);
        User user = userOptional.get();
        user.setFirstName(Objects.requireNonNullElse(dto.getFirstName(), user.getFirstName()));
        user.setLastName(Objects.requireNonNullElse(dto.getLastName(), user.getLastName()));
        user.setEmail(Objects.requireNonNullElse(dto.getEmail(), user.getEmail()));
        user.setUpdatedAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))));
        dao.update(user);
        return new Response("Successfully updated");
    }

    @Override
    public Response delete(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        return dao.delete(id) ? new Response("Successfully deleted") : new Response("Something went wrong");
    }

    @Override
    public Response get(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        Optional<User> optionalUser = dao.getById(id);
        return optionalUser.map(user -> new Response(user, "User found"))
                .orElseThrow(() -> new CustomizedException("User not found", 404));
    }

    @Override
    public Response getAll() {
        return new Response(dao.getAll(), "Success");
    }

    @Override
    public void confirmAccount(String userId, String code) {
        BaseUtil.checkParam(userId, "Id");
        BaseUtil.checkParam(code, "Code");
        Optional<User> optionalUser = dao.getById(userId);
        if (optionalUser.isEmpty()) throw new CustomizedException("User not found", 404);
        User user = optionalUser.get();
        if (!Objects.equals(code, user.getCode())) throw new CustomizedException("Bad request", 400);
        user.setStatus(UserStatus.ACTIVE);
        dao.update(user);
    }

    @Override
    public Cookie createCookie(String userId, String role) {
        Cookie auth = new Cookie("auth", role + "/" +userId);
        auth.setPath("/");
        auth.setMaxAge(300);
        return auth;
    }

    @Override
    public User checkLogin(LoginRequest loginDto) {
        Optional<User> optionalUser = dao.getByEmail(loginDto.email());
        if (optionalUser.isEmpty()) throw new UserNotFoundException("User not found", 404);
        User user = optionalUser.get();
        if (user.getStatus().getLevel() != 0) throw new CustomizedException("Account is not active", 403);
        if (!util.match(loginDto.password(), user.getPassword())) throw new CustomizedException("Bad credentials", 400);
        return user;
    }

    @Override
    public void forgotPassword(String email) {
        Optional<User> byEmail = dao.getByEmail(email);
        if (byEmail.isEmpty()) throw new UserNotFoundException("User not found", 404);
        User user = byEmail.get();
        String generatedPassword = String.valueOf(new Random().nextInt(99999, 1000000));
        Map<String, String> body = Map.of(
                "subject", "Temporary password",
                "content", generatedPassword,
                "email", user.getEmail()
        );
        MailService.getInstance().sendEmail(body);
        dao.updatePassword(util.encode(generatedPassword), user.getId());
    }

    @Override
    public void changePassword(ChangePasswordRequest dto, String userId) {
        Optional<User> optionalUser = dao.getById(userId);
        if (optionalUser.isEmpty()) throw new UserNotFoundException("User not found", 404);
        User user = optionalUser.get();
        if (!util.match(dto.oldPassword(), user.getPassword()))
            throw new CustomizedException("Old password is incorrect", 400);
        dao.updatePassword(util.encode(dto.newPassword()), userId);
    }

    @SneakyThrows
    @Override
    public void setProfileImage(String userId, Part filePart) {
        Attachment attachment = Attachment.childBuilder()
                .fileOriginalNam(filePart.getSubmittedFileName())
                .size(filePart.getSize())
                .mainContent(filePart.getInputStream().readAllBytes())
                .linkedId(userId)
                .linkedName("user")
                .createdBy(userId)
                .createdAt(LocalDateTime.now())
                .build();
        AttachmentDAO.getInstance().save(attachment);
    }

    @Override
    public String getProfileImage(String userId) {
        Optional<Attachment> optionalAttachment = AttachmentDAO.getInstance().getByLinkedId(userId, "user");
        return optionalAttachment.map(attachment -> "data:image/jpg;base64," + Base64.getEncoder().encodeToString(attachment.getMainContent())).orElse(null);
    }

    @Override
    public void changeUserStatus(String userId, UserStatus status) {
        dao.changeUserStatus(userId, status);
    }
}
