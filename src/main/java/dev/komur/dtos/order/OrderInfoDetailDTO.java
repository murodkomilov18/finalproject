package dev.komur.dtos.order;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderInfoDetailDTO {
    private String name;
    private Double price;
    private Integer quantity;

    @Builder(builderMethodName = "childBuilder")
    public OrderInfoDetailDTO(String name, Double price, Integer quantity) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }
}
