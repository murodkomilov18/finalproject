<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 09-07-2023
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.util.Base64" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Details</title>
    <link rel="stylesheet" href="../resources/css/admin_dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<section>
    <div class="container">
        <div class="row justify-content-center">
            <h1 class="mb-4">Order Detail</h1>
        </div>
        <div class="row float-right">
            <div class="col-lg-3">
                <a href="${previous_link}" class="btn btn-primary btn-sm mb-3">Back</a>
            </div>
        </div>
        <table class="table table-striped table-bordered text-center">
            <thead class="table-dark">
            <tr>
                <th>№</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${details}" var="detail">
                <tr>
                    <td>${i = i + 1}</td>
                    <td>${detail.getName()}</td>
                    <td>${detail.getPrice()}</td>
                    <td>${detail.getQuantity()}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>
</body>
</html>
