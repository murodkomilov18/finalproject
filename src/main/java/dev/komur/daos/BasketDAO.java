package dev.komur.daos;

import dev.komur.dtos.order.OrderDetailDTO;
import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.entities.Basket;
import dev.komur.entities.User;
import dev.komur.enums.OrderStatus;
import dev.komur.enums.PaymentStatus;
import dev.komur.exceptions.CustomizedException;
import dev.komur.exceptions.UserNotFoundException;
import dev.komur.mapper.BasketMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BasketDAO extends DAO<Basket, String> {
    private static volatile BasketDAO instance;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private final BasketMapper mapper = BasketMapper.getInstance();
    private static final String INSERT_TO_BASKET_QUERY = """
            insert into basket(user_id, created_at, created_by) values(?, ?, ?) returning id;
            """;

    private static final String UPDATE_BASKET_DETAIL_QUERY = """
            update basket set user_id= ?, updated_at = ?, updated_by = ? where id = ?;
            """;

    private static final String GET_ORDER_DETAILS = """
            select bd.id, a.main_content, f.name, f.price, bd.quantity from basket_detail bd
            left join basket b on b.id = bd.basket_id
            left join users u on u.id = b.user_id
            left join food f on bd.food_id = f.id
            left join attachment a on a.linked_id::int = f.id
            where a.linked_name = 'food' and bd.is_deleted is false and u.id = ?;
            """;

    private static final String GET_ACTIVE_ORDERS = """
            select b.id, sum(cast(bd.quantity as numeric) * cast(f.price as numeric)) as total, u.firstname, u.lastname, u.id as user_id, b.order_status
            from basket b
                     left join basket_detail bd on b.id = bd.basket_id
                     left join food f on f.id = bd.food_id
                     left join users u on b.user_id = u.id
            where b.is_deleted = true and b.order_status = 'ACTIVE'
              and b.order_time > current_timestamp + interval '5 hours'
            group by b.id, u.firstname, u.lastname, u.id, b.order_status order by b.id limit ? offset ?;
            """;

    private static final String GET_USER_ORDER_HISTORY = """
            select b.id, sum(cast(bd.quantity as numeric) * cast(f.price as numeric)) as total, b.payment_status, b.rating
            from basket b
            left join basket_detail bd on b.id = bd.basket_id
            left join food f on f.id = bd.food_id
            where b.user_id = ? and b.is_deleted is true and b.order_status = 'PICKED'
            group by b.payment_status, b.id, b.rating limit ? offset ?;
            """;

    private static final String GET_USERS_FEEDBACK = """
            select b.id, u.firstname, u.lastname, b.rating, b.feedback
            from basket b
            left join users u on u.id = b.user_id
            where b.is_deleted is true and b.rating is not null and feedback is not null;
            """;

    public static BasketDAO getInstance() {
        if (instance == null) {
            synchronized (BasketDAO.class){
                if (instance == null){
                    instance = new BasketDAO();
                }
            }
        }
        return instance;
    }
    @Override
    public Basket save(Basket basket) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_TO_BASKET_QUERY)) {
            pr.setString(1, basket.getUserId());
            pr.setDate(2, Date.valueOf(basket.getCreatedAt().toLocalDate()));
            pr.setString(3, basket.getCreatedBy());

            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                basket.setId(String.valueOf(resultSet.getInt("id")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving the category in database");
        }
        return basket;
    }

    @Override
    public Optional<Basket> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from basket where id = ?;")) {
            pr.setInt(1 , Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Basket basket = mapper.fromResultSet(resultSet);
                return Optional.of(basket);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the basket from database");
        }
        return Optional.empty();
    }

    @Override
    public List<Basket> getAll() {
        Connection connection = getConnection();
        List<Basket> baskets;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from basket where is_deleted = false;");
            baskets = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all baskets from database");
        }
        return baskets;
    }

    @Override
    public int update(Basket basket) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_BASKET_DETAIL_QUERY)){
            pr.setString(1, basket.getUserId());
            pr.setDate(2, Date.valueOf(LocalDate.now()));
            pr.setString(3, basket.getUpdatedBy());
            pr.setInt(4, Integer.parseInt(basket.getId()));
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the basket");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket set is_deleted = true where id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            return pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the basket");
        }
    }

    public String existByUserId(String userId){
        if (hasActiveOrder(userId)) throw new CustomizedException("You have active order. Please order later", 403);
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select id from basket where is_deleted = false and user_id = ?;")){
            pr.setString(1, userId);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                return String.valueOf(resultSet.getInt("id"));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while checking the basket");
        }
        return null;
    }

    private boolean hasActiveOrder(String userId){
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select order_time from basket where is_deleted is true and user_id = ? order by order_time desc limit 1;")){
            pr.setString(1, userId);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                LocalDateTime orderTime = resultSet.getObject("order_time", LocalDateTime.class);
                return orderTime.isAfter(LocalDateTime.now());
            }
            return false;
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while checking the active order");
        }
    }
    
//    public Basket getByUserId(String userId){
//        Connection connection = getConnection();
//        try (PreparedStatement pr = connection.prepareStatement("select * from basket where user_id = ?;")){
//            pr.setString(1, userId);
//            ResultSet resultSet = pr.executeQuery();
//            if(resultSet.next()){
//                return mapper.fromResultSet(resultSet);
//            }
//        }catch (SQLException e){
//            logger.info(e.getMessage());
//            throw new RuntimeException("Something went wrong while checking the basket");
//        }
//        return null;
//    }

    public int getCountAllItemsInBasket(String userId){
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select count(*) as number from basket_detail bd left join basket b on b.id = bd.basket_id left join users u on u.id = b.user_id where bd.is_deleted is false and u.id = ?;")){
            pr.setString(1, userId);
            ResultSet resultSet = pr.executeQuery();
            if(resultSet.next()){
                return Integer.parseInt(resultSet.getString("number"));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while checking the basket");
        }
        return 0;
    }

    public List<OrderDetailDTO> getOrderDetailsByUserId(String userId) {
        Connection connection = getConnection();
        List<OrderDetailDTO> details;
        try(PreparedStatement pr = connection.prepareStatement(GET_ORDER_DETAILS)) {
            pr.setString(1, userId);
            ResultSet resultSet = pr.executeQuery();
            details = mapper.fromResultSetToOrderDetailList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting basket details from database");
        }
        return details;
    }

    public void deleteOrderDetail(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("delete from basket_detail where id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the order from basket");
        }
    }

    public String setOrderTime(String userId, LocalDateTime dateTime) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket set order_time = ?, order_status = ?, is_deleted = true where is_deleted = false and user_id = ? returning id;")){
            pr.setObject(1, dateTime, Types.TIMESTAMP);
            pr.setString(2, OrderStatus.ACTIVE.name());
            pr.setString(3, userId);
            ResultSet resultSet = pr.executeQuery();
            int id = 0;
            if (resultSet.next()){
                id = resultSet.getInt("id");
            }
            if (id != 0) changeOrderStatus(id);
            if (Duration.between(LocalDateTime.now(), dateTime).toHours() >= 2) addLoyaltyPoints(userId);
            return String.valueOf(id);
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while setting time to the order");
        }
    }

    private void addLoyaltyPoints(String userId){
        UserDAO userDAO = UserDAO.getInstance();
        User user = userDAO.getById(userId).orElseThrow(() -> new UserNotFoundException("User not found", 404));
        user.setLoyaltyPoints(user.getLoyaltyPoints() + 50);
        userDAO.update(user);
    }

    private void changeOrderStatus(int id){
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket_detail set is_deleted = true where basket_id = ?;")){
            pr.setInt(1, id);
            pr.executeUpdate();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while changing the order status");
        }
    }

    public void changePaymentStatus(PaymentStatus status, String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket set payment_status = ? where id = ?;")){
            pr.setString(1, status.name());
            pr.setInt(2, Integer.parseInt(id));
            pr.executeUpdate();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while changing the payment status");
        }
    }

    public List<OrderInfoDTO> getActiveOrders(int recordsPerPage, int offset) {
        List<OrderInfoDTO> infos = new ArrayList<>();
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement(GET_ACTIVE_ORDERS)){
            pr.setInt(1, recordsPerPage);
            pr.setInt(2, offset);
            ResultSet resultSet = pr.executeQuery();
            while (resultSet.next()){
                infos.add(OrderInfoDTO.childBuilder()
                                .userId(resultSet.getString("user_id"))
                                .firstName(resultSet.getString("firstname"))
                                .lastName(resultSet.getString("lastname"))
                                .total(resultSet.getDouble("total"))
                                .orderStatus(resultSet.getString("order_status"))
                                .id(String.valueOf(resultSet.getInt("id")))
                                .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while retrieving all active orders");
        }
        return infos;
    }

    public int getNumberOfAllActiveOrders(){
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select count(*) as total from basket where is_deleted = true and order_status = 'ACTIVE' and order_time < current_timestamp + interval '5 hours';")){
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()) return resultSet.getInt("total");
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all active orders");
        }
        return 0;
    }

    public List<OrderInfoDetailDTO> getOrderInfoDetails(String id) {
        List<OrderInfoDetailDTO> details = new ArrayList<>();
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select f.name, f.price, bd.quantity from basket_detail bd left join food f on f.id = bd.food_id where basket_id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            while (resultSet.next()){
                details.add(OrderInfoDetailDTO.childBuilder()
                                .name(resultSet.getString("name"))
                                .price(resultSet.getDouble("price"))
                                .quantity(resultSet.getInt("quantity"))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting order detail");
        }
        return details;
    }

    public void changeOrderStatus(String id, OrderStatus status, PaymentStatus paymentStatus) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket set order_status = ?, payment_status = ? where id = ?;")){
            pr.setString(1, status.name());
            pr.setString(2, paymentStatus.name());
            pr.setInt(3, Integer.parseInt(id));
            pr.executeUpdate();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while changing the order status");
        }
    }

    public List<OrderInfoDTO> getUserOrderHistory(int recordsPerPage, int offset, String userId) {
        List<OrderInfoDTO> infos = new ArrayList<>();
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement(GET_USER_ORDER_HISTORY)){
            pr.setString(1, userId);
            pr.setInt(2, recordsPerPage);
            pr.setInt(3, offset);
            ResultSet resultSet = pr.executeQuery();
            while (resultSet.next()){
                infos.add(OrderInfoDTO.childBuilder()
                        .total(resultSet.getDouble("total"))
                        .orderStatus(resultSet.getString("payment_status"))
                        .id(String.valueOf(resultSet.getInt("id")))
                                .rating(resultSet.getString("rating"))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting user order history");
        }
        return infos;
    }

    public int getNumberOfUserOrders(String userId) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select count(*) as total from basket where is_deleted = true and order_status = 'PICKED' and user_id = ?;")){
            pr.setString(1, userId);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()) return resultSet.getInt("total");
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the number of user orders");
        }
        return 0;
    }

    public void saveRateAndFeedback(String basketId, String rating, String commentText) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update basket set rating = ?, feedback = ? where id = ?;")){
            pr.setString(1, rating);
            pr.setString(2, commentText);
            pr.setInt(3, Integer.parseInt(basketId));
            pr.executeUpdate();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving order rate and feedback");
        }
    }

    public List<OrderInfoDTO> getUserFeedbacks() {
        List<OrderInfoDTO> infos = new ArrayList<>();
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement(GET_USERS_FEEDBACK)){
            ResultSet resultSet = pr.executeQuery();
            while (resultSet.next()){
                infos.add(OrderInfoDTO.childBuilder()
                        .id(String.valueOf(resultSet.getInt("id")))
                        .firstName(resultSet.getString("firstname"))
                        .lastName(resultSet.getString("lastname"))
                        .rating(resultSet.getString("rating"))
                        .feedback(resultSet.getString("feedback"))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting users feedback");
        }
        return infos;
    }
}
