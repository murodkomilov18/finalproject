package dev.komur.mapper;

import dev.komur.dtos.food.FoodCreateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.entities.Food;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FoodMapper implements BaseMapper<Food, ResultSet, FoodCreateDTO>{
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static volatile FoodMapper mapper;

    public static FoodMapper getInstance() {
        if (mapper == null) {
            synchronized (FoodMapper.class){
                if (mapper == null){
                    mapper = new FoodMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public Food fromCreateDTO(@NonNull FoodCreateDTO dto) {
        return Food.childBuilder()
                .name(dto.name())
                .price(dto.price())
                .categoryId(dto.categoryId())
                .createdBy(dto.createdBy())
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public Food fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return Food.childBuilder()
                    .id(resultSet.getString("id"))
                    .name(resultSet.getString("name"))
                    .price(resultSet.getDouble("price"))
                    .categoryId(String.valueOf(resultSet.getInt("category_id")))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<Food> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<Food> foods = new ArrayList<>();
        try {
            while (resultSet.next()){
                foods.add(Food.childBuilder()
                        .id(resultSet.getString("id"))
                        .name(resultSet.getString("name"))
                        .price(resultSet.getDouble("price"))
                        .categoryId(String.valueOf(resultSet.getInt("category_id")))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return foods;
    }
}
