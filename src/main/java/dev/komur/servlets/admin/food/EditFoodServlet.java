package dev.komur.servlets.admin.food;

import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.category.CategoryService;
import dev.komur.services.category.CategoryServiceImpl;
import dev.komur.services.food.FoodServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "EditFoodServlet", value = "/food/edit/*")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class EditFoodServlet extends HttpServlet {
    private final CategoryService categoryService = CategoryServiceImpl.getInstance();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Response all = categoryService.getAll();
        request.setAttribute("categories", all.getData());
        String foodId = request.getRequestURI().substring(11);
        Response category = categoryService.getFoodCategory(foodId);
        request.setAttribute("foodCategory", category.getData());
        request.getSession().setAttribute("foodId", foodId);
        request.setAttribute("pageHeader", "Edit Food");
        request.getRequestDispatcher("/admin/food/add_food.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        String foodId = request.getSession().getAttribute("foodId").toString();
        final String categoryId = request.getParameter("category_id");
        final String name = request.getParameter("f_name");
        boolean hasName = Objects.isNull(name) || name.isBlank();
        boolean hasPrice = true;
        double price = 0;
        try {
            price = Double.parseDouble(request.getParameter("f_price"));
            if (price <= 0) hasPrice = false;
        }catch (Exception e){
            hasPrice = false;
        }
        boolean hasImage = request.getPart("f_image").getSize() > 0;

        FoodUpdateDTO dto = new FoodUpdateDTO();
        dto.setId(foodId);
        dto.setUpdatedBy(userId);
        if (!hasName) dto.setName(name);
        if (hasPrice) dto.setPrice(price);
        if (hasImage){
            Part part = request.getPart("f_image");
            dto.setFileOriginalName(part.getSubmittedFileName());
            dto.setSize(part.getSize());
            dto.setMainContent(part.getInputStream().readAllBytes());
        }
        String foodCategoryId = categoryService.getFoodCategory(foodId).getData().toString();
        if (!Objects.equals(foodCategoryId, categoryId))
            dto.setCategoryId(categoryId);

        Response result = FoodServiceImpl.getInstance().update(dto);
        request.setAttribute("message", result.getMessage());
        request.setAttribute("next_link", request.getSession().getAttribute("previous_link"));
        request.getRequestDispatcher("/payload/success.jsp").forward(request, response);
    }
}
