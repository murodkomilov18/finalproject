package dev.komur.dtos.food;

import dev.komur.dtos.GenericDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class CategoryUpdateDTO extends GenericDto {
    private String name;
    private String updatedBy;
    private String fileOriginalName;
    private Long size;
    private byte[] mainContent;


    @Builder(builderMethodName = "childBuilder")
    public CategoryUpdateDTO(String id, String name, String updatedBy, String fileOriginalName, Long size, byte[] mainContent) {
        super(id);
        this.name = name;
        this.updatedBy = updatedBy;
        this.fileOriginalName = fileOriginalName;
        this.size = size;
        this.mainContent = mainContent;
    }
}
