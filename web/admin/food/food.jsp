<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 09-07-2023
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Menu Items</title>
    <link rel="stylesheet" href="../../resources/css/admin_dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<section>
    <div class="sidebar">
        <h3 class="logo">Admin <br>Dashboard</h3>
        <ul class="admin_navbar">
            <li><a href="/admin"><i class="fa fa-windows"></i> Dashboard</a></li>
            <li><a href="/active_orders"><i class="fa fa-shopping-bag"></i> Orders</a></li>
            <li class="active"><a href="/food"><i class="fa fa-pie-chart"></i> Menu</a></li>
            <li><a href="/category"><i class="fa fa-cube"></i> Category</a></li>
            <li><a href="/feedbacks"><i class="fa fa-comments"></i> Users Feedback</a></li>
            <li><a href="/auth/change_password"><i class="fa fa-cog"></i> Change Password</a></li>
            <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Log out</a></li>
        </ul>
    </div>
    <div class="main">
        <div class="row justify-content-center">
            <h1 class="mb-4">Menu Items</h1>
        </div>
        <div class="row float-right">
            <div class="col-lg-3">
                <a href="/add_food" class="btn btn-primary btn-sm mb-3">Add Food To Menu</a>
            </div>
        </div>
        <table class="table table-striped table-bordered text-center">
            <thead class="table-dark">
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${products}" var="product">
                <tr>
                    <td><img style="width: 75px" src="${product.getFileOriginalName()}" alt="Image"></td>
                    <td>${product.getName()}</td>
                    <td>$${product.getPrice()}</td>
                    <td>${product.getCategoryId()}</td>
                    <td>
                        <a href="/food/edit/${product.getId()}" class="btn btn-primary">Update</a>
                        <a href="/food/${product.getId()}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="row">
            <nav>
                <ul class="pagination justify-content-start">
                    <c:if test="${currentPage != 1}">
                        <li class="page-item">
                            <a class="page-link" href="/food?page=${currentPage - 1}" tabindex="-1">Previous</a>
                        </li>
                    </c:if>
                    <c:forEach begin="1" end="${noOfPages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <li class="page-item disabled"><a class="page-link" href="/food?page=${i}">${i}</a></li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item"><a class="page-link" href="/food?page=${i}">${i}</a></li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    <c:if test="${currentPage lt noOfPages}">
                        <li class="page-item"><a class="page-link" href="/food?page=${currentPage + 1}">Next</a></li>
                    </c:if>
                </ul>
            </nav>
        </div>
    </div>
</section>
</body>
</html>
