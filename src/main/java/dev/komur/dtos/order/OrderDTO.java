package dev.komur.dtos.order;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    private List<OrderDetailDTO> orderDetails;
    private Double total;
}
