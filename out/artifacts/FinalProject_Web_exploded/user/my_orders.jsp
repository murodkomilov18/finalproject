<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 24-07-2023
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>My Orders</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div>
        <div class="basket-header"><a href="/user"><span class="close">&times;</span></a></div>
        <div class="row justify-content-center">
            <h1 class="mb-4">My Orders</h1>
        </div>
        <table class="table table-striped table-bordered text-center">
            <thead class="table-dark">
            <tr>
                <th>№</th>
                <th>Total price</th>
                <th>Order Status</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${infos}" var="info">
                <tr>
                    <td>${i = i + 1}</td>
                    <td>$${info.getTotal()}</td>
                    <td>${info.getOrderStatus()}</td>
                    <td>
                        <a href="/my_order/detail?id=${info.getId()}" class="btn btn-primary">Details</a>
                        <c:choose>
                            <c:when test="${info.getRating() == null}">
                                <a href="/my_order/feedback?id=${info.getId()}" class="btn btn-primary">Evaluate</a>
                            </c:when>
                            <c:otherwise>
                                <a href="/my_order/feedback?id=${info.getId()}" class="btn btn-primary disabled">Evaluated</a>
                            </c:otherwise>
                        </c:choose>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <c:if test="${noOfPages != 1}">
            <div class="row">
                <nav>
                    <ul class="pagination justify-content-start">
                        <c:if test="${currentPage != 1}">
                            <li class="page-item">
                                <a class="page-link" href="/my_orders?page=${currentPage - 1}" tabindex="-1">Previous</a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${noOfPages}" var="i">
                            <c:choose>
                                <c:when test="${currentPage eq i}">
                                    <li class="page-item disabled"><a class="page-link" href="/my_orders?page=${i}">${i}</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item"><a class="page-link" href="/my_orders?page=${i}">${i}</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                        <c:if test="${currentPage lt noOfPages}">
                            <li class="page-item"><a class="page-link" href="/my_orders?page=${currentPage + 1}">Next</a></li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </c:if>
    </div>
</div>
</body>
</html>
