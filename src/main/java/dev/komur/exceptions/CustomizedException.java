package dev.komur.exceptions;

public class CustomizedException extends RuntimeException{
    private final Integer code;

    public CustomizedException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
