package dev.komur.servlets.admin.user;

import dev.komur.enums.UserStatus;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "UserBanServlet", value = "/user/ban")
public class UserBanServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("id");
        UserServiceImpl.getInstance().changeUserStatus(userId, UserStatus.BANNED);
        response.sendRedirect("/admin");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
