package dev.komur.config;

import java.util.Properties;

public class AppConfig {
    public static Properties getSMTPConfiguration() {
        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "587");
        String username = "murodkomilov18@gmail.com";
        String password = "";
        prop.put("username", username);
        prop.put("password", password);
        return prop;
    }
}
