package dev.komur.utils;

import lombok.NonNull;
import org.mindrot.jbcrypt.BCrypt;

import java.util.Objects;
import java.util.UUID;

public class BaseUtil {

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public String encode(@NonNull String rawPassword) {
        return BCrypt.hashpw(rawPassword, BCrypt.gensalt());
    }

    public boolean match(@NonNull String rawPassword, @NonNull String encodedPassword) {
        return BCrypt.checkpw(rawPassword, encodedPassword);
    }

    public static void checkParam(String param, String paramName) {
        if (Objects.isNull(param) || param.isBlank())
            throw new IllegalArgumentException(paramName + " can not be null or empty");
    }
}
