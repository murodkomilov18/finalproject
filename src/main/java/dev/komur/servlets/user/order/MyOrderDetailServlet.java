package dev.komur.servlets.user.order;

import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "MyOrderDetailServlet", value = "/my_order/detail")
public class MyOrderDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String basketId = request.getParameter("id");
        List<OrderInfoDetailDTO> details = BasketServiceImpl.getInstance().getOrderInfoDetail(basketId);
        request.setAttribute("details", details);
        request.setAttribute("previous_link", "/my_orders");
        request.getRequestDispatcher("/admin/orders_detail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
