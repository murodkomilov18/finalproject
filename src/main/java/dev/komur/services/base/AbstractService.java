package dev.komur.services.base;


import dev.komur.daos.BaseDao;
import dev.komur.mapper.Mapper;
import dev.komur.utils.BaseUtil;

public class AbstractService<R extends BaseDao,
        M extends Mapper> {
    protected final R dao;
    protected final M mapper;
    protected final BaseUtil util;

    public AbstractService(R dao, M mapper) {
        this.dao = dao;
        this.mapper = mapper;
        this.util = new BaseUtil();
    }
}
