package dev.komur.dtos.attachment;

import dev.komur.dtos.GenericDto;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AttachmentDTO extends GenericDto {
    private String fileOriginalName;
    private Long size;
    private byte[] mainContent;
    private String linkedId;
    private String linkedName;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;

    @Builder(builderMethodName = "childBuilder")
    public AttachmentDTO(String id, String fileOriginalName, Long size, byte[] mainContent, String linkedId, String linkedName, String createdAt, String updatedAt, String createdBy, String updatedBy) {
        super(id);
        this.fileOriginalName = fileOriginalName;
        this.size = size;
        this.mainContent = mainContent;
        this.linkedId = linkedId;
        this.linkedName = linkedName;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }
}
