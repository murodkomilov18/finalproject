<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Sign in Form </title>
        <link rel="stylesheet" href="../resources/css/style.css">
                        
    </head>
    <body>
        <section class="container forms">
            <div class="form login">
                <div class="form-content">
                    <header>Sign in</header>
                    <form action="${pageContext.request.contextPath}/auth/login" method="post">
                        <div class="field input-field email">
                            <input type="email" placeholder="Email" class="input" name="email">
                        </div>

                        <div class="field input-field password">
                            <input type="password" placeholder="Password" class="password" name="password">
                            <i class='bx bx-hide eye-icon'></i>
                        </div>

                        <div class="form-link">
                            <a href="/auth/forgot_password" class="forgot-pass">Forgot password?</a>
                        </div>

                        <div class="field button-field">
                            <button>Login</button>
                        </div>
                    </form>

                    <div class="form-link">
                        <span>Don't have an account? <a href="${pageContext.request.contextPath}/auth/register" class="link signup-link">Sign up</a></span>
                    </div>
                </div>
            </div>
        </section>

        <script src="../resources/script/auth.js"></script>
    </body>
</html>