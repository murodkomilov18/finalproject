package dev.komur.entities;

import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Attachment extends BaseEntity {
    private String fileOriginalName;
    private Long size;
    private byte[] mainContent;
    private String linkedId;
    private String linkedName;

    @Builder(builderMethodName = "childBuilder")
    public Attachment(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, @NonNull String fileOriginalNam, @NonNull Long size, byte[] mainContent, @NonNull String linkedId, @NonNull String linkedName) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.fileOriginalName = fileOriginalNam;
        this.size = size;
        this.mainContent = mainContent;
        this.linkedId = linkedId;
        this.linkedName = linkedName;
    }
}
