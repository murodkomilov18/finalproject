package dev.komur.dtos.basket;

import dev.komur.dtos.GenericDto;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BasketUpdateDTO extends GenericDto {
    private String userId;
    private String updatedBy;

    @Builder(builderMethodName = "childBuilder")
    public BasketUpdateDTO(String id, String userId, String updatedBy) {
        super(id);
        this.userId = userId;
        this.updatedBy = updatedBy;
    }
}
