package dev.komur.daos;

import dev.komur.entities.BasketDetail;
import dev.komur.entities.Category;
import dev.komur.mapper.BasketDetailMapper;
import dev.komur.mapper.CategoryMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BasketDetailDAO extends DAO<BasketDetail, String> {
    private static volatile BasketDetailDAO instance;
    private final BasketDetailMapper mapper = BasketDetailMapper.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final String INSERT_BASKET_DETAIL_QUERY = """
            insert into basket_detail(food_id, quantity, created_at, created_by, basket_id) values(?, ?, ?, ?, ?) returning id;
            """;

    private static final String UPDATE_BASKET_DETAIL_QUERY = """
            update basket_detail set quantity= ?, updated_at = ?, updated_by = ? where id = ?;
            """;

    public static BasketDetailDAO getInstance() {
        if (instance == null) {
            synchronized (BasketDetailDAO.class){
                if (instance == null){
                    instance = new BasketDetailDAO();
                }
            }
        }
        return instance;
    }
    @Override
    public BasketDetail save(BasketDetail detail) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_BASKET_DETAIL_QUERY)) {
            pr.setInt(1, Integer.parseInt(detail.getFoodId()));
            pr.setInt(2, detail.getQuantity());
            pr.setDate(3, Date.valueOf(detail.getCreatedAt().toLocalDate()));
            pr.setString(4, detail.getCreatedBy());
            pr.setInt(5, Integer.parseInt(detail.getBasketId()));

            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                detail.setId(String.valueOf(resultSet.getInt("id")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving basket details in database");
        }
        return detail;
    }

    @Override
    public Optional<BasketDetail> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from basket_detail where id = ?;")) {
            pr.setInt(1 , Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                BasketDetail detail = mapper.fromResultSet(resultSet);
                return Optional.of(detail);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting basket detail from database");
        }
        return Optional.empty();
    }

    @Override
    public List<BasketDetail> getAll() {
        Connection connection = getConnection();
        List<BasketDetail> details;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from basket_detail where is_deleted = false;");
            details = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all basket details from database");
        }
        return details;
    }

    @Override
    public int update(BasketDetail detail) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_BASKET_DETAIL_QUERY)){
            pr.setInt(1, detail.getQuantity());
            pr.setDate(2, Date.valueOf(LocalDate.now()));
            pr.setString(3, detail.getUpdatedBy());
            pr.setInt(4, Integer.parseInt(detail.getId()));
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the basket detail");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("delete from basket_detail where id = ?;")) {
            pr.setInt(1, Integer.parseInt(id));
            return pr.execute();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the basket detail");
        }
    }

    public Optional<BasketDetail> getByBasketAndFoodId(String basketId, String foodId){
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("select * from basket_detail where food_id = ? and basket_id = ?;")) {
            pr.setInt(1, Integer.parseInt(foodId));
            pr.setInt(2, Integer.parseInt(basketId));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                BasketDetail detail = mapper.fromResultSet(resultSet);
                return Optional.of(detail);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the basket detail");
        }
        return Optional.empty();
    }
}
