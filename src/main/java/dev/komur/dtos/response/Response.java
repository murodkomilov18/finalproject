package dev.komur.dtos.response;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Response {
    private Object data;
    private String message;

    public Response(String message) {
        this.message = message;
    }
}
