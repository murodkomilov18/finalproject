package dev.komur.servlets.auth;

import dev.komur.services.user.UserService;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ActivateServlet", value = "/auth/check")
public class ActivateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String code = request.getParameter("code");
        UserService userService = UserServiceImpl.getInstance();
        userService.confirmAccount(userId, code);
        PrintWriter writer = response.getWriter();
        writer.write("<div style=\"display:flex;justify-content:center;flex-direction:column;align-items:center\">" +
                "  <h3>Your account has been activated. Now you can login to your account</h3>" +
                "  <a href=\"http://localhost:8080/auth/login\" style=\"width:100px;background-color:lime;padding:20px 30px;text-decoration:none;font-weight:bold\">Back to Login</a>" +
                "</div>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
