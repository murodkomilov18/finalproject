package dev.komur.servlets.auth;

import dev.komur.dtos.response.Response;
import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.services.user.UserService;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "RegisterServlet", value = "/auth/register")
public class RegisterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/auth/register.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = UserServiceImpl.getInstance();
        UserCreateDTO dto = (UserCreateDTO) request.getAttribute("userCreateDto");
        Response result = userService.create(dto);
        request.setAttribute("message", result.getMessage());
        request.setAttribute("next_link", "/auth/login");
        request.getRequestDispatcher("/payload/success.jsp").forward(request, response);
    }
}
