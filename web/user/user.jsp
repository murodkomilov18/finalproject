<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 04-07-2023
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>User page</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
<div class="app-container">
    <div class="sidebar">
        <div class="account-info">
            <div class="account-info-picture">
                <a href="/set_image">
                    <c:choose>
                        <c:when test="${image == null}">
                            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp&f=y" alt="Account">
                        </c:when>
                        <c:otherwise>
                            <img src="${image}" alt="Profile picture">
                        </c:otherwise>
                    </c:choose>
                </a>
            </div>
            <div class="account-info-name">${user.getFirstName()} ${user.getLastName()}</div>
        </div>
        <ul class="sidebar-list">
            <c:forEach items="${categories}" var="category">
                <c:choose>
                    <c:when test="${categoryId==category.getId()}">
                        <li class="sidebar-list-item active">
                            <a href="/user?category=${category.getId()}">
                                <img src="${category.getFileOriginalName()}" alt="Category img">
                                <span>${category.getName()}</span>
                            </a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="sidebar-list-item">
                            <a href="/user?category=${category.getId()}">
                                <img src="${category.getFileOriginalName()}" alt="Category img">
                                <span>${category.getName()}</span>
                            </a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </ul>
    </div>
    <div class="app-content">
        <div class="app-content-header">
            <h1 class="app-content-headerText">Menu:</h1>
            <div class="app-content-header-nav">
                <a class="app-content-headerButton-transparent" href="/auth/change_password">Change Password</a>
                <a class="app-content-headerButton-transparent" href="/my_orders">Evaluate my orders</a>
                <div class="loyalty">
                    <button class="loyalty_btn" title="Client Loyalty Points(When you order 2hours before for receiving order, 50 loyalty points are given)"><svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512"><path d="M173.8 5.5c11-7.3 25.4-7.3 36.4 0L228 17.2c6 3.9 13 5.8 20.1 5.4l21.3-1.3c13.2-.8 25.6 6.4 31.5 18.2l9.6 19.1c3.2 6.4 8.4 11.5 14.7 14.7L344.5 83c11.8 5.9 19 18.3 18.2 31.5l-1.3 21.3c-.4 7.1 1.5 14.2 5.4 20.1l11.8 17.8c7.3 11 7.3 25.4 0 36.4L366.8 228c-3.9 6-5.8 13-5.4 20.1l1.3 21.3c.8 13.2-6.4 25.6-18.2 31.5l-19.1 9.6c-6.4 3.2-11.5 8.4-14.7 14.7L301 344.5c-5.9 11.8-18.3 19-31.5 18.2l-21.3-1.3c-7.1-.4-14.2 1.5-20.1 5.4l-17.8 11.8c-11 7.3-25.4 7.3-36.4 0L156 366.8c-6-3.9-13-5.8-20.1-5.4l-21.3 1.3c-13.2 .8-25.6-6.4-31.5-18.2l-9.6-19.1c-3.2-6.4-8.4-11.5-14.7-14.7L39.5 301c-11.8-5.9-19-18.3-18.2-31.5l1.3-21.3c.4-7.1-1.5-14.2-5.4-20.1L5.5 210.2c-7.3-11-7.3-25.4 0-36.4L17.2 156c3.9-6 5.8-13 5.4-20.1l-1.3-21.3c-.8-13.2 6.4-25.6 18.2-31.5l19.1-9.6C65 70.2 70.2 65 73.4 58.6L83 39.5c5.9-11.8 18.3-19 31.5-18.2l21.3 1.3c7.1 .4 14.2-1.5 20.1-5.4L173.8 5.5zM272 192a80 80 0 1 0 -160 0 80 80 0 1 0 160 0zM1.3 441.8L44.4 339.3c.2 .1 .3 .2 .4 .4l9.6 19.1c11.7 23.2 36 37.3 62 35.8l21.3-1.3c.2 0 .5 0 .7 .2l17.8 11.8c5.1 3.3 10.5 5.9 16.1 7.7l-37.6 89.3c-2.3 5.5-7.4 9.2-13.3 9.7s-11.6-2.2-14.8-7.2L74.4 455.5l-56.1 8.3c-5.7 .8-11.4-1.5-15-6s-4.3-10.7-2.1-16zm248 60.4L211.7 413c5.6-1.8 11-4.3 16.1-7.7l17.8-11.8c.2-.1 .4-.2 .7-.2l21.3 1.3c26 1.5 50.3-12.6 62-35.8l9.6-19.1c.1-.2 .2-.3 .4-.4l43.2 102.5c2.2 5.3 1.4 11.4-2.1 16s-9.3 6.9-15 6l-56.1-8.3-32.2 49.2c-3.2 5-8.9 7.7-14.8 7.2s-11-4.3-13.3-9.7z"/></svg>
                        ${user.getLoyaltyPoints()}
                    </button>
                </div>
                <a class="app-content-headerButton" href="/basket">
                    <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512"><path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z"/></svg>
                    <c:choose>
                        <c:when test="${count == 0}">
                            <span class="badge inactive">${count}</span>
                        </c:when>
                        <c:otherwise>
                            <span class="badge">${count}</span>
                        </c:otherwise>
                    </c:choose>
                </a>
                <a style="margin-left: 20px" class="app-content-headerButton" href="/auth/logout">Logout</a>
            </div>
        </div>
        <div class="products-area-wrapper gridView">
            <c:forEach items="${products}" var="product">
                <div class="products-row">
                    <div class="product-cell image">
                        <img src="${product.getFileOriginalName()}" alt="product">
                    </div>
                    <div><span class="span">${product.getName()}</span></div>
                    <div class="product-cell price">$${product.getPrice()}</div>
                    <div class="btn"><a href="/order?id=${product.getId()}">Order</a></div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>
