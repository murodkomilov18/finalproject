package dev.komur.servlets.user.order;

import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "MyOrdersServlet", value = "/my_orders")
public class MyOrdersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        int page = 1;
        int recordsPerPage = 5;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));

        BasketService service = BasketServiceImpl.getInstance();
        List<OrderInfoDTO> infoDTOS = service.getUserOrderHistory(recordsPerPage, (page-1) * recordsPerPage, userId);
        int noOfRecords = service.getNumberOfUserOrders(userId);
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        request.setAttribute("infos", infoDTOS);
        request.getRequestDispatcher("/user/my_orders.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
