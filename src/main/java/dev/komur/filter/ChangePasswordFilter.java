package dev.komur.filter;

import dev.komur.dtos.user.ChangePasswordRequest;
import dev.komur.exceptions.CustomizedException;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.Objects;

@WebFilter(filterName = "ChangePasswordFilter", urlPatterns = "/auth/change_password")
public class ChangePasswordFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String oldPassword = req.getParameter("old_password");
        String newPassword = req.getParameter("new_password");
        String confirmPassword = req.getParameter("confirm_password");

        if (req.getMethod().equalsIgnoreCase("post")){
            if (Objects.isNull(oldPassword) || oldPassword.isBlank()
                || Objects.isNull(newPassword) || newPassword.isBlank()
                || Objects.isNull(confirmPassword) || confirmPassword.isBlank())
                throw new CustomizedException("Input parameters are not valid", 400);
            if (Objects.equals(oldPassword, newPassword))
                throw new CustomizedException("Old and new passwords can not be same", 400);
            if (!Objects.equals(newPassword, confirmPassword))
                throw new CustomizedException("New and confirm password should be same", 400);

            req.setAttribute("changePasswordDto", new ChangePasswordRequest(oldPassword, newPassword));
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
