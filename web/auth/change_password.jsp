<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 04-07-2023
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Change Password </title>
  <link rel="stylesheet" href="../resources/css/style.css">
</head>
<body>
  <section class="container forms">
    <div class="form login">
      <div class="form-content">
        <header>Change password</header>
        <form action="${pageContext.request.contextPath}/auth/change_password" method="post">
          <div class="field input-field password">
            <input type="password" placeholder="Enter old password" class="password" name="old_password">
            <i class='bx bx-hide eye-icon'></i>
          </div>

          <div class="field input-field password">
            <input type="password" placeholder="Enter new password" class="password" name="new_password">
            <i class='bx bx-hide eye-icon'></i>
          </div>
          <div class="field input-field password">
            <input type="password" placeholder="Confirm new password" class="password" name="confirm_password">
            <i class='bx bx-hide eye-icon'></i>
          </div>
          <div class="field button-field">
            <button>Submit</button>
          </div>
        </form>
      </div>
    </div>
  </section>
  <script src="../resources/script/auth.js"></script>
</body>
</html>
