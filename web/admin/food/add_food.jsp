<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 09-07-2023
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <title>Add Food</title>
  <link rel="stylesheet" href="../../resources/css/style.css">
</head>
<body>
<section class="container forms">
  <div class="form login">
    <div class="form-content">
      <header>${pageHeader}</header>
      <form enctype='multipart/form-data' method="post">
        <div class="field input-field">
          <select name="category_id" class="select">
            <c:forEach items="${categories}" var="category">
              <c:choose>
                <c:when test="${foodCategory == category.getId()}">
                  <option class="input" name="category" selected="selected" value="${category.getId()}">
                    <c:out value="${category.getName()}"/>
                  </option>
                </c:when>
                <c:otherwise>
                  <option class="input" name="category" value="${category.getId()}">
                    <c:out value="${category.getName()}"/>
                  </option>
                </c:otherwise>
              </c:choose>
            </c:forEach>
          </select>
        </div>

        <div class="field input-field">
          <input type="text" placeholder="Item name" name="f_name">
        </div>

        <div class="field input-field">
          <input type="number" step=0.01 placeholder="Item price" name="f_price">
        </div>

        <div class="field input-field">
          <input type="file" name="f_image">
        </div>
        <div class="field button-field">
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  </div>
</section>
</body>
</html>
