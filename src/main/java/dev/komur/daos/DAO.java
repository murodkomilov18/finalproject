package dev.komur.daos;

import dev.komur.entities.Domain;
import org.postgresql.Driver;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public abstract class DAO<T extends Domain, ID extends Serializable> implements BaseDao {
    private final ThreadLocal<Connection> connectionThreadLocal = ThreadLocal.withInitial(
            () -> {
                try {
                    DriverManager.registerDriver(new Driver());
                    return DriverManager.getConnection(
                            "jdbc:postgresql://localhost:5432/cafe",
                            "postgres",
                            ""
                    );
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
    );

    protected abstract T save(T t);

    protected abstract Optional<T> getById(ID id);
    protected abstract List<T> getAll();

    protected abstract int update(T t);

    protected abstract boolean delete(ID id);

    protected Connection getConnection() {
        return connectionThreadLocal.get();
    }
}
