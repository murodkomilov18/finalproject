package dev.komur.servlets.admin.order;

import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "OrderDetailServlet", value = "/orders/detail")
public class OrdersDetailServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        List<OrderInfoDetailDTO> details = BasketServiceImpl.getInstance().getOrderInfoDetail(id);
        request.setAttribute("details", details);
        request.setAttribute("previous_link", "/active_orders");
        request.getRequestDispatcher("/admin/orders_detail.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
