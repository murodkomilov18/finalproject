package dev.komur.enums;

public enum OrderStatus {
        ACTIVE,
        NOT_PICKED,
        PICKED;
    }