package dev.komur.mapper;

import dev.komur.dtos.food.CategoryCreateDTO;
import dev.komur.entities.Category;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CategoryMapper implements BaseMapper<Category, ResultSet, CategoryCreateDTO>{
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static volatile CategoryMapper mapper;

    public static CategoryMapper getInstance() {
        if (mapper == null) {
            synchronized (CategoryMapper.class){
                if (mapper == null){
                    mapper = new CategoryMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public Category fromCreateDTO(@NonNull CategoryCreateDTO dto) {
        return Category.childBuilder()
                .name(dto.name())
                .createdBy(dto.createdBy())
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public Category fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return Category.childBuilder()
                    .id(resultSet.getString("id"))
                    .name(resultSet.getString("name"))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<Category> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<Category> categories = new ArrayList<>();
        try {
            while (resultSet.next()){
                categories.add(Category.childBuilder()
                        .id(resultSet.getString("id"))
                        .name(resultSet.getString("name"))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return categories;
    }
}
