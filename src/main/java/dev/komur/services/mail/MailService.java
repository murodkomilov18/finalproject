package dev.komur.services.mail;

import dev.komur.config.AppConfig;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MailService {
    private static MailService instance;
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final String EMAIL_FROM = "noreply@gmail.com";
    private static final Properties PROPERTIES = AppConfig.getSMTPConfiguration();


    public void sendEmail(Map<String, String> body) {
        Session session = Session.getInstance(PROPERTIES, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        PROPERTIES.getProperty("username"),
                        PROPERTIES.getProperty("password"));
            }
        });

        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(EMAIL_FROM));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(body.get("email"),
                            false));
            msg.setSubject(body.get("subject"));
            msg.setContent(body.get("content"), "text/html");
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while sending email");
        }
    }

    public static MailService getInstance() {
        if (instance == null) {
            instance = new MailService();
        }
        return instance;
    }


}
