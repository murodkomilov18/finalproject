<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 21-07-2023
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Basket Details</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
    <div class="app-container modal">
        <div class="basket-row">
            <div class="basket-header"><a href="/user"><span class="close">&times;</span></a></div>
            <c:forEach items="${details}" var="detail">
                <div class="wrapper">
                    <img src="${detail.image()}" alt="image">
                    <span class="span">${detail.name()}</span>
                    <span class="span">$${detail.price()}</span>
                    <span class="span">${detail.quantity()}</span>
                    <a class="app-content-headerButton-danger" href="/basket?id=${detail.id()}">Remove</a>
                </div>
            </c:forEach>
            <div class="wrapper">
                <span class="span">Total:</span>
                <span class="span">$${total}</span>
            </div>
            <form action="/basket?total=${total}" class="wrapper_1" method="post">
                <div>
                    <label class="span" for="time">Select time to receive order:</label>
                    <input id="time" type="time" name="time" value="00:00">
                </div>
                <div><button class="button" type="submit">Order</button></div>
            </form>
        </div>
    </div>
</body>
</html>
