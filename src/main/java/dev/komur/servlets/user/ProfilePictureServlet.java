package dev.komur.servlets.user;

import dev.komur.exceptions.CustomizedException;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "ProfilePictureServlet", value = "/set_image")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class ProfilePictureServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/user/profile_image.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        if (request.getPart("image").getSize() <= 0)
            throw new CustomizedException("No image is selected", 400);
        final Part filePart = request.getPart("image");

        UserServiceImpl.getInstance().setProfileImage(userId, filePart);
        response.sendRedirect("/user");
    }
}
