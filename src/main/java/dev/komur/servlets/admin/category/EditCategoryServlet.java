package dev.komur.servlets.admin.category;

import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.category.CategoryServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "EditCategoryServlet", value = "/category/edit/*")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024, // 1 MB
        maxFileSize = 1024 * 1024 * 10,      // 10 MB
        maxRequestSize = 1024 * 1024 * 100   // 100 MB
)
public class EditCategoryServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String categoryId = request.getRequestURI().substring(15);
        request.getSession().setAttribute("categoryId", categoryId);
        request.setAttribute("pageHeader", "Edit Category");
        request.getRequestDispatcher("/admin/category/add_category.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        String categoryId = request.getSession().getAttribute("categoryId").toString();
        final String name = request.getParameter("f_name");
        boolean hasName = Objects.isNull(name) || name.isBlank();
        boolean hasFile = request.getPart("f_image").getSize() <= 0;

        CategoryUpdateDTO dto;
        if (hasFile && hasName)
            throw new CustomizedException("Input parameters are invalid", 400);

        final Part filePart = request.getPart("f_image");
        if (hasName)
            dto = CategoryUpdateDTO.childBuilder().id(categoryId).updatedBy(userId)
                    .fileOriginalName(filePart.getSubmittedFileName()).size(filePart.getSize())
                    .mainContent(filePart.getInputStream().readAllBytes()).build();
        else if (hasFile)
            dto = CategoryUpdateDTO.childBuilder().id(categoryId).updatedBy(userId).name(name).build();
        else
            dto = CategoryUpdateDTO.childBuilder().id(categoryId).name(name).updatedBy(userId)
                    .fileOriginalName(filePart.getSubmittedFileName()).size(filePart.getSize())
                    .mainContent(filePart.getInputStream().readAllBytes()).build();

        Response result = CategoryServiceImpl.getInstance().update(dto);
        request.setAttribute("message", result.getMessage());
        request.setAttribute("next_link", request.getSession().getAttribute("previous_link"));
        request.getRequestDispatcher("/payload/success.jsp").forward(request, response);
    }
}
