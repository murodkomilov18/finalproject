<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 09-07-2023
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Food Category</title>
    <link rel="stylesheet" href="../../resources/css/style.css">
</head>
<body>
<section class="container forms">
    <div class="form login">
        <div class="form-content">
            <header>${pageHeader}</header>
            <form enctype='multipart/form-data' method="post">
                <div class="field input-field">
                    <input type="text" name="f_name" placeholder="Category name">
                </div>

                <div class="field input-field">
                    <input type="file" name="f_image">
                </div>

                <div class="field button-field">
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</section>
</body>
</html>
