package dev.komur.dtos.user;

import dev.komur.dtos.Dto;
public record UserCreateDTO(String firstName, String lastName, String email, String password, String confirmPassword) implements Dto {
}
