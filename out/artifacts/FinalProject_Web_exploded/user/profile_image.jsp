<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 23-07-2023
  Time: 11:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Change Profile Picture</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
    <div class="app-container modal">
        <div class="basket-row">
            <div class="basket-header"><a href="#"><span class="close">&times;</span></a></div>
            <form class="wrapper" method="post" enctype="multipart/form-data">
                <div>
                    <label class="span" for="img">Select profile picture:</label>
                    <input id="img" type="file" name="image">
                </div>
                <div><button class="button" type="submit">Set</button></div>
            </form>
        </div>
    </div>
</body>
</html>
