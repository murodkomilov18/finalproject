<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 04-07-2023
  Time: 14:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title> Forgot Password </title>
  <link rel="stylesheet" href="../resources/css/style.css">
</head>
<body>
  <section class="container forms">
    <div class="form login">
      <div class="form-content">
        <header>Forgot password</header>
        <form action="${pageContext.request.contextPath}/auth/forgot_password" method="post">
          <div class="field input-field email">
            <input type="email" placeholder="Enter your email" class="input" name="email">
          </div>

          <div class="field button-field">
            <button>Submit</button>
          </div>
        </form>
      </div>
    </div>
  </section>
</body>
</html>
