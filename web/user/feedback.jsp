<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 17-07-2023
  Time: 08:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order Feedback</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
<div class="app-container modal">
    <div class="products-row">
        <div class="basket-header"><a style="padding-left: 250px" href="/my_orders"><span class="close">&times;</span></a></div>
        <form class="modal" method="post" action="/my_order/feedback?id=${id}">
            <label class="span">Please rate the order:</label>
            <span class="star-rating">
                    <input type="radio" name="rating1" value="1"><i></i>
                    <input type="radio" name="rating1" value="2"><i></i>
                    <input type="radio" name="rating1" value="3"><i></i>
                    <input type="radio" name="rating1" value="4"><i></i>
                    <input type="radio" name="rating1" value="5"><i></i>
                </span>
            <textarea maxlength="250" cols="75" name="commentText" rows="5"></textarea>
            <button class="app-content-headerButton" type="submit">Submit</button>
        </form>
    </div>
</div>
</body>
</html>
