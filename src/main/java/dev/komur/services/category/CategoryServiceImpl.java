package dev.komur.services.category;

import dev.komur.config.AppContext;
import dev.komur.daos.AttachmentDAO;
import dev.komur.daos.CategoryDAO;
import dev.komur.dtos.food.CategoryCreateDTO;
import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.entities.Attachment;
import dev.komur.entities.Category;
import dev.komur.exceptions.CustomizedException;
import dev.komur.mapper.CategoryMapper;
import dev.komur.services.base.AbstractService;
import dev.komur.utils.BaseUtil;
import lombok.NonNull;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Logger;

public class CategoryServiceImpl extends AbstractService<CategoryDAO, CategoryMapper> implements CategoryService {
    private static volatile CategoryService service;
    private final AttachmentDAO attachmentDAO = AttachmentDAO.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    public CategoryServiceImpl(CategoryDAO dao, CategoryMapper mapper) {
        super(dao, mapper);
    }

    public static CategoryService getInstance() {
        if (service == null) {
            synchronized (CategoryServiceImpl.class){
                if (service == null){
                    service = new CategoryServiceImpl(
                            AppContext.getBean(CategoryDAO.class),
                            AppContext.getBean(CategoryMapper.class)
                    );
                }
            }
        }
        return service;
    }

    @Override
    public Response create(@NonNull CategoryCreateDTO dto) {
        if(dao.existsByName(dto.name()))
            throw new CustomizedException("%s is already exist".formatted(dto.name()), 403);
        try {
            Category category = mapper.fromCreateDTO(dto);
            Category savedcategory = dao.save(category);
            Attachment attachment = Attachment.childBuilder()
                    .fileOriginalNam(dto.fileOriginalName())
                    .size(dto.size())
                    .mainContent(dto.mainContent())
                    .linkedId(savedcategory.getId())
                    .linkedName("category")
                    .createdBy(dto.createdBy())
                    .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                    .build();
            attachmentDAO.save(attachment);
            return new Response(savedcategory.getId(), "Category is successfully created and added to database");
        } catch (Exception e) {
            logger.severe(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Response update(@NonNull CategoryUpdateDTO dto) {
        BaseUtil.checkParam(dto.getId(), "Id");
        Optional<Category> optionalCategory = dao.getById(dto.getId());
        if (optionalCategory.isEmpty())
            throw new CustomizedException("Category is not found", 404);
        Category category = optionalCategory.get();
        category.setName(Objects.requireNonNullElse(dto.getName(), category.getName()));
        category.setUpdatedBy(Objects.requireNonNullElse(dto.getUpdatedBy(), category.getUpdatedBy()));
        category.setUpdatedAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))));

        if (dto.getFileOriginalName() != null){
            Optional<Attachment> optionalAttachment = attachmentDAO.getByLinkedId(dto.getId(), "category");
            if (optionalAttachment.isEmpty())
                throw new CustomizedException("Attachment is empty", 400);
            Attachment attachment = optionalAttachment.get();
            attachment.setFileOriginalName(dto.getFileOriginalName());
            attachment.setSize(dto.getSize());
            attachment.setMainContent(dto.getMainContent());
            attachment.setUpdatedBy(dto.getUpdatedBy());
            attachmentDAO.update(attachment);
        }
        dao.update(category);
        return new Response("Successfully updated");
    }

    @Override
    public Response delete(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        return dao.delete(id) ? new Response("Successfully deleted") : new Response("Something went wrong");
    }

    @Override
    public Response get(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        Optional<Category> optionalCategory = dao.getById(id);
        return optionalCategory.map(category -> new Response(category, "Category found"))
                .orElseThrow(() -> new CustomizedException("Category not found", 404));
    }

    @Override
    public Response getAll() {
        return new Response(getCategoriesList(dao.getAll()), "Success");
    }

    @Override
    public Response getFoodCategory(@NonNull String foodId) {
        BaseUtil.checkParam(foodId, "Food id");
        Optional<Category> optionalCategory = dao.getFoodCategory(foodId);
        return optionalCategory.map(category -> new Response(category.getId(), "Category found"))
                .orElseThrow(() -> new CustomizedException("Category not found", 404));
    }

    @Override
    public List<CategoryUpdateDTO> getCategoriesByPage(int recordsPerPage, int offset) {
        List<Category> categories = dao.getByPage(recordsPerPage, offset);
        return getCategoriesList(categories);
    }

    private List<CategoryUpdateDTO> getCategoriesList(List<Category> categories) {
        List<CategoryUpdateDTO> all = new ArrayList<>();
        categories.forEach(category -> {
            Attachment attachment = attachmentDAO.getByLinkedId(category.getId(), "category").orElseThrow(
                    () -> new CustomizedException("%s image is not found".formatted(category.getName()), 404));
            all.add(CategoryUpdateDTO.childBuilder().id(category.getId())
                    .name(category.getName()).size(attachment.getSize()).mainContent(attachment.getMainContent())
                    .fileOriginalName("data:image/jpg;base64,"+ Base64.getEncoder().encodeToString(attachment.getMainContent()))
                    .build());
        });
        return all;
    }

    @Override
    public int getNumberOfAllFoods() {
        return dao.getAll().size();
    }
}
