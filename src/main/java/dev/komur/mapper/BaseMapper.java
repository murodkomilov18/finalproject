package dev.komur.mapper;

import dev.komur.dtos.Dto;
import dev.komur.entities.Domain;
import lombok.NonNull;

import java.sql.ResultSet;
import java.util.List;

public interface BaseMapper<T extends Domain, RS extends ResultSet, CD extends Dto> extends Mapper{
    T fromCreateDTO(@NonNull CD dto);

    T fromResultSet(@NonNull RS resultSet);
    List<T> fromResultSetToList(@NonNull RS resultSet);

}
