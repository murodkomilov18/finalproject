<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 17-07-2023
  Time: 08:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order Quantity</title>
    <link rel="stylesheet" href="../resources/css/dashboard.css">
</head>
<body>
<div class="app-container modal">
    <div class="products-row">
        <div class="wrapper1">
            <div class="header">
                <a href="/user"><span class="close">&times;</span></a>
            </div>
            <div class="product-cell">
                <img src="${product.getFileOriginalName()}" alt="product">
            </div>
        </div>
        <form action="/order?id=${product.getId()}" method="post">
            <div class="form">
                <label class="span" for="quantity">Quantity:</label>
                <input id="quantity" type="number" min="1" max="5" name="quantity" value="1">
            </div>
            <button class="button" type="submit">Add to Order</button>
        </form>
    </div>
</div>
</body>
</html>
