package dev.komur.servlets.auth;

import dev.komur.dtos.user.ChangePasswordRequest;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "ChangePasswordServlet", value = "/auth/change_password")
public class ChangePasswordServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/auth/change_password.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ChangePasswordRequest dto = (ChangePasswordRequest) request.getAttribute("changePasswordDto");
        String userId = request.getSession().getAttribute("userId").toString();
        UserServiceImpl.getInstance().changePassword(dto, userId);
        response.sendRedirect("/auth/logout");
    }
}
