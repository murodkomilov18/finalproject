package dev.komur.filter.security;


import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.logging.Logger;

@Log
@WebFilter(urlPatterns = "/*")
public class SecurityFilter implements Filter {
    private final Logger logger = Logger.getLogger(getClass().getName());

    private static final List<String> WHITE_LIST = List.of(
            "/",
            "/auth/login",
            "/auth/check",
            "/auth/forgot_password",
            "/resources/.+",
            "/auth/register"
    );
    private static final List<String> ADMIN_LIST = List.of(
            "/admin",
            "/food",
            "/food/edit/*",
            "/food/*",
            "/add_food",
            "/add_category",
            "/category/*",
            "/category/edit/*",
            "/category",
            "/user/ban",
            "/user/unblock",
            "/active_orders",
            "/orders/detail",
            "/active_orders/picked",
            "/feedbacks"
    );

    private static final Predicate<String> isSecure = (uri) -> WHITE_LIST.stream().noneMatch(uri::matches);
    private static final Predicate<String> isAdmin = (uri) -> ADMIN_LIST.stream().anyMatch(uri::matches);

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String requestURI = request.getRequestURI();
        log.info(requestURI);
        if (!isSecure.test(requestURI))
            chain.doFilter(request, response);
        else {
            Cookie[] cookies = Objects.requireNonNullElse(request.getCookies(), new Cookie[]{});
            Arrays.stream(cookies)
                    .filter(cookie -> cookie.getName().equals("auth"))
                    .findFirst()
                    .ifPresentOrElse((cookie -> {
                        try {
                            HttpSession session = request.getSession();
                            String role = cookie.getValue().split("/")[0];
                            if (Objects.equals(role, "CLIENT") && isAdmin.test(requestURI)){
                                request.setAttribute("message", "Permission denied");
                                request.setAttribute("code", 401);
                                request.getRequestDispatcher("/payload/error.jsp").forward(request, response);
                            }
                            String userId = cookie.getValue().split("/")[1];
                            session.setAttribute("userId", userId);
                            chain.doFilter(request, response);
                        } catch (IOException | ServletException e) {
                            logger.info(e.getMessage());
                            throw new RuntimeException(e.getLocalizedMessage());
                        }
                    }), () -> {
                        try {
                            response.sendRedirect("/auth/login?next=" + requestURI);
                        } catch (IOException e) {
                            logger.info(e.getMessage());
                            throw new RuntimeException(e.getLocalizedMessage());
                        }
                    });
        }
    }
}
