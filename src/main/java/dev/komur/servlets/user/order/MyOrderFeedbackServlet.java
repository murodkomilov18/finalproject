package dev.komur.servlets.user.order;

import dev.komur.exceptions.CustomizedException;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "MyOrderFeedbackServlet", value = "/my_order/feedback")
public class MyOrderFeedbackServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("id", request.getParameter("id"));
        request.getRequestDispatcher("/user/feedback.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String basketId = request.getParameter("id");
        String rating = request.getParameter("rating1");
        String commentText = request.getParameter("commentText");
        if (Objects.isNull(basketId) || basketId.isBlank())
            throw new CustomizedException("Order should be selected", 400);
        if (Objects.isNull(rating) || rating.isBlank())
            throw new CustomizedException("Please rate the order", 400);
        if (Objects.isNull(commentText) || commentText.isBlank())
            throw new CustomizedException("Please send feedback about the order", 400);

        BasketServiceImpl.getInstance().saveRateAndFeedback(basketId, rating, commentText);
        response.sendRedirect("/user");
    }
}
