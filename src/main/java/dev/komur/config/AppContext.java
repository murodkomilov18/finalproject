package dev.komur.config;

import dev.komur.daos.BasketDAO;
import dev.komur.daos.CategoryDAO;
import dev.komur.daos.FoodDAO;
import dev.komur.daos.UserDAO;
import dev.komur.exceptions.BeanNotFoundException;
import dev.komur.mapper.BasketMapper;
import dev.komur.mapper.CategoryMapper;
import dev.komur.mapper.FoodMapper;
import dev.komur.mapper.UserMapper;
import dev.komur.services.user.UserServiceImpl;

public class AppContext {
    public static <T> T getBean(Class<T> clazz) {
        return getBean(clazz.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName) {
        return switch (beanName) {
            case "UserDAO" -> (T) UserDAO.getInstance();
            case "FoodDAO" -> (T) FoodDAO.getInstance();
            case "CategoryDAO" -> (T) CategoryDAO.getInstance();
            case "BasketDAO" -> (T) BasketDAO.getInstance();
            case "UserMapper" -> (T) UserMapper.getInstance();
            case "FoodMapper" -> (T) FoodMapper.getInstance();
            case "CategoryMapper" -> (T) CategoryMapper.getInstance();
            case "BasketMapper" -> (T) BasketMapper.getInstance();
            case "UserService" -> (T) UserServiceImpl.getInstance();
            default -> throw new BeanNotFoundException("Bean not found with name %S".formatted(beanName), 404);
        };
    }
}
