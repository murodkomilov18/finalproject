package dev.komur.servlets.user.order;

import dev.komur.daos.UserDAO;
import dev.komur.entities.User;
import dev.komur.enums.PaymentStatus;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Objects;

@WebServlet(name = "PaymentServlet", value = "/pay")
public class PaymentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BasketService service = BasketServiceImpl.getInstance();
        String userId = request.getSession().getAttribute("userId").toString();
        String total = request.getParameter("total");
        String id = request.getParameter("id");
        if (Objects.isNull(id)) throw new CustomizedException("Order not found", 404);
        if (Objects.isNull(total)){
            service.changePaymentStatus(PaymentStatus.WILL_BE_PAID_BY_CASH, id);
        }else {
            double orderAmount;
            try {
                orderAmount = Double.parseDouble(total);
            }catch (Exception e){
                throw new RuntimeException(e.getLocalizedMessage());
            }
            User user = (User) UserServiceImpl.getInstance().get(userId).getData();
            if (user.getAccount() < orderAmount) throw new CustomizedException("You don't have enough amount in your account", 400);
            BigDecimal subtract = BigDecimal.valueOf(user.getAccount()).subtract(BigDecimal.valueOf(orderAmount));
            user.setAccount(subtract.doubleValue());
            UserDAO.getInstance().update(user);
            service.changePaymentStatus(PaymentStatus.PAID_BY_ACCOUNT, id);
        }
        response.sendRedirect("/user");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
