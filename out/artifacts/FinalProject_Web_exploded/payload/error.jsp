<html lang="en">
<head>
  <link
          href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'
  />
  <link href="../resources/css/payload.css" rel="stylesheet" />
  <title>Error</title>
</head>
<body>
<%
  Object message = request.getAttribute("message");
  Object code = request.getAttribute("code");
%>
<div class="popup" id="error">
  <div class="popup-content">
    <div class="imgbox">
      <img src="../resources/img/cancel.png" alt="" class="img">
    </div>
    <div class="title">
      <h3>Error code: <%= code.toString() %></h3>
    </div>
    <p class="para"><%= message.toString() %></p>
    <form action="">
      <a href="/auth/login" class="button" id="e_button">TRY AGAIN</a>
    </form>
  </div>
</div>
</body>
</html>