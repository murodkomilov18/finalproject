package dev.komur.services.category;

import dev.komur.dtos.food.CategoryCreateDTO;
import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.entities.Category;
import dev.komur.services.base.GenericCrudService;

import java.util.List;
import java.util.Optional;

public interface CategoryService extends GenericCrudService<CategoryCreateDTO, CategoryUpdateDTO, String> {
    Response getFoodCategory(String foodId);

    List<CategoryUpdateDTO> getCategoriesByPage(int recordsPerPage, int offset);

    int getNumberOfAllFoods();
}
