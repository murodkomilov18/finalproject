package dev.komur.dtos.food;

import dev.komur.dtos.Dto;

public record FoodCreateDTO(String name, Double price, String categoryId, String createdBy, String fileOriginalName, Long size, byte[] mainContent) implements Dto {
}
