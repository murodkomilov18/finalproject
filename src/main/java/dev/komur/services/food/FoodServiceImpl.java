package dev.komur.services.food;

import dev.komur.config.AppContext;
import dev.komur.daos.AttachmentDAO;
import dev.komur.daos.CategoryDAO;
import dev.komur.daos.FoodDAO;
import dev.komur.dtos.food.CategoryUpdateDTO;
import dev.komur.dtos.food.FoodCreateDTO;
import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.entities.Attachment;
import dev.komur.entities.Category;
import dev.komur.entities.Food;
import dev.komur.exceptions.CustomizedException;
import dev.komur.mapper.FoodMapper;
import dev.komur.services.base.AbstractService;
import dev.komur.utils.BaseUtil;
import lombok.NonNull;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Logger;

public class FoodServiceImpl extends AbstractService<FoodDAO, FoodMapper> implements FoodService{
    private static volatile FoodService service;
    private final AttachmentDAO attachmentDAO = AttachmentDAO.getInstance();
    private final CategoryDAO categoryDAO = CategoryDAO.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    public FoodServiceImpl(FoodDAO dao, FoodMapper mapper) {
        super(dao, mapper);
    }

    public static FoodService getInstance() {
        if (service == null) {
            synchronized (FoodServiceImpl.class){
                if (service == null){
                    service = new FoodServiceImpl(
                            AppContext.getBean(FoodDAO.class),
                            AppContext.getBean(FoodMapper.class)
                    );
                }
            }
        }
        return service;
    }

    @Override
    public Response create(@NonNull FoodCreateDTO dto) {
        try {
            Food food = mapper.fromCreateDTO(dto);
            Food savedFood = dao.save(food);
            Attachment attachment = Attachment.childBuilder()
                    .fileOriginalNam(dto.fileOriginalName())
                    .size(dto.size())
                    .mainContent(dto.mainContent())
                    .linkedId(savedFood.getId())
                    .linkedName("food")
                    .createdBy(dto.createdBy())
                    .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                    .build();
            attachmentDAO.save(attachment);
            return new Response(savedFood.getId(), "Food is successfully created and added to database");
        } catch (Exception e) {
            logger.severe(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Response update(@NonNull FoodUpdateDTO dto) {
        BaseUtil.checkParam(dto.getId(), "Id");
        Optional<Food> optionalFood = dao.getById(dto.getId());
        if (optionalFood.isEmpty())
            throw new CustomizedException("Food not found", 404);
        Food food = optionalFood.get();
        food.setName(Objects.requireNonNullElse(dto.getName(), food.getName()));
        food.setPrice(Objects.requireNonNullElse(dto.getPrice(), food.getPrice()));
        food.setCategoryId(dto.getCategoryId());
        food.setUpdatedBy(dto.getUpdatedBy());
        food.setUpdatedAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))));

        if (dto.getFileOriginalName() != null){
            Optional<Attachment> optionalAttachment = attachmentDAO.getByLinkedId(dto.getId(), "food");
            if (optionalAttachment.isEmpty())
                throw new CustomizedException("Attachment is empty", 400);
            Attachment attachment = optionalAttachment.get();
            attachment.setFileOriginalName(dto.getFileOriginalName());
            attachment.setSize(dto.getSize());
            attachment.setMainContent(dto.getMainContent());
            attachment.setUpdatedBy(dto.getUpdatedBy());
            attachmentDAO.update(attachment);
        }
        dao.update(food);
        return new Response("Successfully updated");
    }

    @Override
    public Response delete(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        return dao.delete(id) ? new Response("Successfully deleted") : new Response("Something went wrong");
    }

    @Override
    public Response get(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        Optional<Food> optionalFood = dao.getById(id);
        return optionalFood.map(food -> new Response(food, "Food found"))
                .orElseThrow(() -> new CustomizedException("Food not found", 404));
    }

    @Override
    public Response getAll() {
        return new Response(getFoodList(dao.getAll()), "Success");
    }


    @Override
    public List<FoodUpdateDTO> getFoodsByPage(int recordsPerPage, int offset) {
        List<Food> foods = dao.getByPage(recordsPerPage, offset);
        return getFoodList(foods);
    }

    private List<FoodUpdateDTO> getFoodList(List<Food> foods) {
        List<FoodUpdateDTO> all = new ArrayList<>();

        foods.forEach(food -> {
            Category category = categoryDAO.getById(food.getCategoryId()).orElseThrow(
                    () -> new CustomizedException("Category with id %s is not found".formatted(food.getId()), 404));
            Attachment attachment = attachmentDAO.getByLinkedId(food.getId(), "food").orElseThrow(
                    () -> new CustomizedException("%s image is not found".formatted(food.getName()), 404));
            all.add(FoodUpdateDTO.childBuilder().id(food.getId())
                    .name(food.getName()).price(food.getPrice()).size(attachment.getSize()).mainContent(attachment.getMainContent())
                    .fileOriginalName("data:image/jpg;base64,"+ Base64.getEncoder().encodeToString(attachment.getMainContent()))
                    .categoryId(category.getName()).build());
        });
        return all;
    }

    @Override
    public int getNumberOfAllFoods() {
        return dao.getAll().size();
    }

    @Override
    public List<FoodUpdateDTO> getFoodsByCategory(String categoryId) {
        List<Food> foods = dao.getByCategory(categoryId);
        return getFoodList(foods);
    }

    @Override
    public FoodUpdateDTO getWithImage(String id) {
        Optional<Food> byId = dao.getById(id);
        if (byId.isEmpty()) throw new CustomizedException("Food is not found", 404);
        Food food = byId.get();
        Category category = categoryDAO.getById(food.getCategoryId()).orElseThrow(
                () -> new CustomizedException("Category with id %s is not found".formatted(food.getId()), 404));
        Attachment attachment = attachmentDAO.getByLinkedId(food.getId(), "food").orElseThrow(
                () -> new CustomizedException("%s image is not found".formatted(food.getName()), 404));
        return FoodUpdateDTO.childBuilder().id(food.getId())
                .name(food.getName()).price(food.getPrice()).size(attachment.getSize()).mainContent(attachment.getMainContent())
                .fileOriginalName("data:image/jpg;base64,"+ Base64.getEncoder().encodeToString(attachment.getMainContent()))
                .categoryId(category.getName()).build();
    }
}
