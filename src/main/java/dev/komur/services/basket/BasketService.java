package dev.komur.services.basket;

import dev.komur.dtos.basket.BasketCreateDTO;
import dev.komur.dtos.basket.BasketUpdateDTO;
import dev.komur.dtos.order.OrderDTO;
import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.enums.OrderStatus;
import dev.komur.enums.PaymentStatus;
import dev.komur.services.base.GenericCrudService;

import java.time.LocalDateTime;
import java.util.List;

public interface BasketService extends GenericCrudService<BasketCreateDTO, BasketUpdateDTO, String> {
    void addItemToBasket(BasketCreateDTO dto, String basketId);
    int getCountAllItemsInBasketByUserId(String userId);
    OrderDTO getOrderDetailsByUserId(String userId);

    void deleteOrderDetail(String id);

    String setOrderTime(String userId, LocalDateTime dateTime);

    void changePaymentStatus(PaymentStatus status, String id);

    List<OrderInfoDTO> getActiveOrders(int recordsPerPage, int offset);

    int getNumberOfAllActiveOrders();

    List<OrderInfoDetailDTO> getOrderInfoDetail(String id);

    void changeOrderStatus(String id, OrderStatus status, PaymentStatus paymentStatus);

    List<OrderInfoDTO> getUserOrderHistory(int recordsPerPage, int offset, String userId);

    int getNumberOfUserOrders(String userId);

    void saveRateAndFeedback(String basketId, String rating, String commentText);

    List<OrderInfoDTO> getUserFeedbacks();

}
