package dev.komur.dtos.user;

public record ChangePasswordRequest(String oldPassword, String newPassword) {
}
