package dev.komur.entities;

import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Category extends BaseEntity {
    private String name;

    @Builder(builderMethodName = "childBuilder")
    public Category(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, @NonNull String name) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.name = name;
    }
}
