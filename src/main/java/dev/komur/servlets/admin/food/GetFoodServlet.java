package dev.komur.servlets.admin.food;

import dev.komur.dtos.food.FoodUpdateDTO;
import dev.komur.dtos.response.Response;
import dev.komur.entities.Food;
import dev.komur.services.category.CategoryServiceImpl;
import dev.komur.services.food.FoodService;
import dev.komur.services.food.FoodServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "GetFoodServlet", value = "/food")
public class GetFoodServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int recordsPerPage = 5;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));

        FoodService service = FoodServiceImpl.getInstance();
        List<FoodUpdateDTO> foods = service.getFoodsByPage(recordsPerPage, (page - 1) * recordsPerPage);
        int noOfRecords = service.getNumberOfAllFoods();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        request.setAttribute("products", foods);
        request.getSession().setAttribute("previous_link", request.getRequestURI());
        request.getRequestDispatcher("/admin/food/food.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
