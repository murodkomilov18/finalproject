package dev.komur.mapper;

import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.entities.User;
import dev.komur.enums.UserRole;
import dev.komur.enums.UserStatus;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserMapper implements BaseMapper<User, ResultSet, UserCreateDTO> {
    private final Logger logger = Logger.getLogger(getClass().getName());

    private static volatile UserMapper mapper;

    public static UserMapper getInstance() {
        if (mapper == null) {
            synchronized (UserMapper.class){
                if (mapper == null){
                    mapper = new UserMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public User fromCreateDTO(@NonNull UserCreateDTO dto) {
        return User.childBuilder()
                .firstName(dto.firstName())
                .password(dto.password())
                .email(dto.email())
                .lastName(dto.lastName())
                .status(UserStatus.NOT_ACTIVE)
                .role(UserRole.CLIENT)
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public User fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return User.childBuilder()
                    .id(resultSet.getString("id"))
                    .firstName(resultSet.getString("firstname"))
                    .lastName(resultSet.getString("lastname"))
                    .email(resultSet.getString("email"))
                    .password(resultSet.getString("password"))
                    .status(UserStatus.valueOf(resultSet.getString("status")))
                    .role(UserRole.valueOf(resultSet.getString("role")))
                    .code(resultSet.getString("code"))
                    .loyaltyPoints(resultSet.getLong("loyalty_points"))
                    .account(resultSet.getDouble("account"))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<User> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<User> users = new ArrayList<>();
        try {
            while (resultSet.next()){
                users.add(User.childBuilder()
                        .id(resultSet.getString("id"))
                        .firstName(resultSet.getString("firstname"))
                        .lastName(resultSet.getString("lastname"))
                        .email(resultSet.getString("email"))
                        .password(resultSet.getString("password"))
                        .status(UserStatus.valueOf(resultSet.getString("status")))
                        .role(UserRole.valueOf(resultSet.getString("role")))
                        .code(resultSet.getString("code"))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return users;
    }

}
