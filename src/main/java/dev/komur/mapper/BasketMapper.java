package dev.komur.mapper;

import dev.komur.dtos.basket.BasketCreateDTO;
import dev.komur.dtos.basket.BasketDetailCreateDTO;
import dev.komur.dtos.order.OrderDetailDTO;
import dev.komur.entities.Basket;
import dev.komur.entities.BasketDetail;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BasketMapper implements BaseMapper<Basket, ResultSet, BasketCreateDTO>{
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static volatile BasketMapper mapper;

    public static BasketMapper getInstance() {
        if (mapper == null) {
            synchronized (BasketMapper.class){
                if (mapper == null){
                    mapper = new BasketMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public Basket fromCreateDTO(@NonNull BasketCreateDTO dto) {
        return Basket.childBuilder()
                .userId(dto.userId())
                .createdBy(dto.createdBy())
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public Basket fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return Basket.childBuilder()
                    .id(String.valueOf(resultSet.getInt("id")))
                    .userId(resultSet.getString("user_id"))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<Basket> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<Basket> baskets = new ArrayList<>();
        try {
            while (resultSet.next()){
                baskets.add(Basket.childBuilder()
                        .id(String.valueOf(resultSet.getInt("id")))
                        .userId(resultSet.getString("user_id"))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return baskets;
    }

    public List<OrderDetailDTO> fromResultSetToOrderDetailList(ResultSet resultSet) {
        List<OrderDetailDTO> details = new ArrayList<>();
        try {
            while (resultSet.next()){
                details.add(new OrderDetailDTO(String.valueOf(resultSet.getInt("id")),
                        "data:image/jpg;base64,"+ Base64.getEncoder().encodeToString(resultSet.getBytes("main_content")),
                        resultSet.getString("name"), resultSet.getDouble("price"), resultSet.getInt("quantity")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return details;
    }
}
