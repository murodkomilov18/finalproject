<html lang="en">
<head>
    <link
            href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'
    />
    <link href="../resources/css/payload.css" rel="stylesheet" />
    <title>Success</title>
</head>
<body>
<%
    String message = request.getAttribute("message").toString();
    String nextLink = request.getAttribute("next_link").toString();
%>
<div id="success" class="popup" >
    <div class="popup-content">
        <div class="imgbox">
            <img src="../resources/img/checked.png" alt="" class="img">
        </div>
        <div class="title">
            <h3>Success!</h3>
        </div>
        <p class="para"><%=message%></p>
        <form action="">
            <a href="<%=nextLink%>" class="button" id="s_button">Back</a>
        </form>
    </div>
</div>
</body>
</html>