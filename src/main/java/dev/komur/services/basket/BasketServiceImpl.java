package dev.komur.services.basket;

import dev.komur.config.AppContext;
import dev.komur.daos.BasketDAO;
import dev.komur.daos.BasketDetailDAO;
import dev.komur.dtos.basket.BasketCreateDTO;
import dev.komur.dtos.basket.BasketDetailCreateDTO;
import dev.komur.dtos.basket.BasketUpdateDTO;
import dev.komur.dtos.order.OrderDTO;
import dev.komur.dtos.order.OrderDetailDTO;
import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.dtos.response.Response;
import dev.komur.entities.Basket;
import dev.komur.entities.BasketDetail;
import dev.komur.enums.OrderStatus;
import dev.komur.enums.PaymentStatus;
import dev.komur.exceptions.CustomizedException;
import dev.komur.mapper.BasketDetailMapper;
import dev.komur.mapper.BasketMapper;
import dev.komur.services.base.AbstractService;
import dev.komur.utils.BaseUtil;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class BasketServiceImpl extends AbstractService<BasketDAO, BasketMapper> implements BasketService {
    private static volatile BasketService service;
    private final BasketDetailDAO basketDetailDAO = BasketDetailDAO.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    public BasketServiceImpl(BasketDAO dao, BasketMapper mapper) {
        super(dao, mapper);
    }

    public static BasketService getInstance() {
        if (service == null) {
            synchronized (BasketServiceImpl.class){
                if (service == null){
                    service = new BasketServiceImpl(
                            AppContext.getBean(BasketDAO.class),
                            AppContext.getBean(BasketMapper.class)
                    );
                }
            }
        }
        return service;
    }

    @Override
    public Response create(@NonNull BasketCreateDTO dto) {
        try {
             Basket basket = mapper.fromCreateDTO(dto);
             Basket savedBasket = dao.save(basket);
             BasketDetailCreateDTO createDTO = new BasketDetailCreateDTO(dto.foodId(), dto.quantity(), dto.createdBy(), savedBasket.getId());
            BasketDetail detail = BasketDetailMapper.getInstance().fromCreateDTO(createDTO);
            basketDetailDAO.save(detail);
            return new Response(savedBasket.getId(), "Food is successfully added to your basket");
        } catch (Exception e) {
            logger.severe(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public Response update(@NonNull BasketUpdateDTO dto) {
        return null;
    }

    @Override
    public Response delete(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        return dao.delete(id) ? new Response("Successfully deleted") : new Response("Something went wrong");
    }

    @Override
    public Response get(@NonNull String id) {
        BaseUtil.checkParam(id, "Id");
        Optional<Basket> optionalBasket = dao.getById(id);
        return optionalBasket.map(basket -> new Response(basket, "Food found"))
                .orElseThrow(() -> new CustomizedException("Food not found", 404));
    }

    @Override
    public Response getAll() {
        return new Response(dao.getAll(), "Success");
    }

    @Override
    public void addItemToBasket(BasketCreateDTO dto, String basketId){
        Optional<BasketDetail> optional = basketDetailDAO.getByBasketAndFoodId(basketId, dto.foodId());
        if (optional.isEmpty()){
            BasketDetailCreateDTO createDTO = new BasketDetailCreateDTO(dto.foodId(), dto.quantity(), dto.createdBy(), basketId);
            BasketDetail detail = BasketDetailMapper.getInstance().fromCreateDTO(createDTO);
            basketDetailDAO.save(detail);
        }else {
            BasketDetail detail = optional.get();
            if (detail.getQuantity()+dto.quantity() > 5) throw new CustomizedException("Max quantity for ordering per item is 5!!!", 400);
            detail.setQuantity(detail.getQuantity()+dto.quantity());
            basketDetailDAO.update(detail);
        }
    }

    @Override
    public int getCountAllItemsInBasketByUserId(String userId) {
        return dao.getCountAllItemsInBasket(userId);
    }

    @Override
    public OrderDTO getOrderDetailsByUserId(String userId) {
        List<OrderDetailDTO> details = dao.getOrderDetailsByUserId(userId);
        BigDecimal total = BigDecimal.ZERO;
        for (OrderDetailDTO detail : details) {
            BigDecimal multiply = BigDecimal.valueOf(detail.price()).multiply(BigDecimal.valueOf(detail.quantity()));
            total = total.add(multiply);
        }
        return new OrderDTO(details, total.doubleValue());
    }

    @Override
    public void deleteOrderDetail(String id) {
        dao.deleteOrderDetail(id);
    }

    @Override
    public String setOrderTime(String userId, LocalDateTime dateTime) {
        return dao.setOrderTime(userId, dateTime);
    }

    @Override
    public void changePaymentStatus(PaymentStatus status, String id) {
        dao.changePaymentStatus(status, id);
    }

    @Override
    public List<OrderInfoDTO> getActiveOrders(int recordsPerPage, int offset) {
        return dao.getActiveOrders(recordsPerPage, offset);
    }

    @Override
    public int getNumberOfAllActiveOrders() {
        return dao.getNumberOfAllActiveOrders();
    }

    @Override
    public List<OrderInfoDetailDTO> getOrderInfoDetail(String id) {
        return dao.getOrderInfoDetails(id);
    }

    @Override
    public void changeOrderStatus(String id, OrderStatus status, PaymentStatus paymentStatus) {
        dao.changeOrderStatus(id, status, paymentStatus);
    }

    @Override
    public List<OrderInfoDTO> getUserOrderHistory(int recordsPerPage, int offset, String userId) {
        return dao.getUserOrderHistory(recordsPerPage, offset, userId);
    }

    @Override
    public int getNumberOfUserOrders(String userId) {
        return dao.getNumberOfUserOrders(userId);
    }

    @Override
    public void saveRateAndFeedback(String basketId, String rating, String commentText) {
        dao.saveRateAndFeedback(basketId, rating, commentText);
    }

    @Override
    public List<OrderInfoDTO> getUserFeedbacks() {
        return dao.getUserFeedbacks();
    }
}
