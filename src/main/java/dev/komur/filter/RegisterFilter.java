package dev.komur.filter;

import dev.komur.daos.UserDAO;
import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.dtos.user.UserDTO;
import dev.komur.exceptions.CustomizedException;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Objects;

@WebFilter(filterName = "RegisterFilter", urlPatterns = "/auth/register")
public class RegisterFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        final String firstname = req.getParameter("firstname");
        final String lastname = req.getParameter("lastname");
        final String email = req.getParameter("email");
        final String password = req.getParameter("password");
        final String confirmPassword = req.getParameter("confirmPassword");

        if (req.getMethod().equalsIgnoreCase("post")){
            if (Objects.isNull(firstname) || firstname.isBlank())
                throw new CustomizedException("Firstname is required", 400);
            if (Objects.isNull(email) || email.isBlank() ||
                !email.matches("^[\\w!#$%&amp;'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&amp;'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"))
                throw new CustomizedException("Email is not valid", 400);
            if (UserDAO.getInstance().existByEmail(email))
                throw new CustomizedException("Provided email is already registered in the system", 406);
            if (Objects.isNull(password) || password.isBlank() || password.contains(" "))
                throw new CustomizedException("Password is not valid", 400);
            if (Objects.isNull(confirmPassword) || confirmPassword.isBlank() || confirmPassword.contains(" "))
                throw new CustomizedException("Confirm password is not valid", 400);
            if (!Objects.equals(password, confirmPassword))
                throw new IllegalArgumentException("Bad request. Passwords mismatched");

            req.setAttribute("userCreateDto", new UserCreateDTO(firstname, lastname, email, password, confirmPassword));
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
