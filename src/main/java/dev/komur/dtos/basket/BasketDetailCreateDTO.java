package dev.komur.dtos.basket;

import dev.komur.dtos.Dto;

public record BasketDetailCreateDTO(String foodId, Integer quantity, String createdBy, String basketId) implements Dto {
}
