package dev.komur.servlets.user.order;

import dev.komur.daos.BasketDAO;
import dev.komur.dtos.basket.BasketCreateDTO;
import dev.komur.exceptions.CustomizedException;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import dev.komur.services.food.FoodServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "OrderServlet", value = "/order")
public class OrderServlet extends HttpServlet {
    private final BasketService service = BasketServiceImpl.getInstance();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String foodId = request.getParameter("id");
        request.setAttribute("product", FoodServiceImpl.getInstance().getWithImage(foodId));
        request.getRequestDispatcher("/user/modal.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userId = request.getSession().getAttribute("userId").toString();
        String s_quantity = request.getParameter("quantity");
        String foodId = request.getParameter("id");
        int quantity;
        try {
            if (Objects.isNull(s_quantity) || s_quantity.isBlank())
                throw new CustomizedException("Quantity is required", 400);
            quantity = Integer.parseInt(s_quantity);
        }catch (Exception e){
            throw new CustomizedException("Quantity should be number", 400);
        }
        if (quantity > 5) throw new CustomizedException("Max quantity for ordering per item is 5!!!", 400);
        if (Objects.isNull(foodId) || foodId.isBlank())
            throw new CustomizedException("Item is not selected", 400);

        BasketCreateDTO dto = new BasketCreateDTO(userId, foodId, quantity, userId);
        String basketId = BasketDAO.getInstance().existByUserId(userId);
        if (basketId != null) {
            service.addItemToBasket(dto, basketId);
        }else {
            service.create(dto);
        }
        response.sendRedirect("/user");
    }
}
