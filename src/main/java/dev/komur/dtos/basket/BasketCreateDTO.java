package dev.komur.dtos.basket;

import dev.komur.dtos.Dto;

public record BasketCreateDTO(String userId,String foodId, Integer quantity, String createdBy) implements Dto {
}
