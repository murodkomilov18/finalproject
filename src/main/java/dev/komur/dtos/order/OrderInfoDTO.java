package dev.komur.dtos.order;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class OrderInfoDTO{
    private String id;
    private String userId;
    private String firstName;
    private String lastName;
    private String orderStatus;
    private Double total;
    private String rating;
    private String feedback;

    @Builder(builderMethodName = "childBuilder")
    public OrderInfoDTO(String id, String userId, String firstName, String lastName,
                        String orderStatus, Double total, String rating, String feedback) {
        this.id = id;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.orderStatus = orderStatus;
        this.total = total;
        this.rating = rating;
        this.feedback = feedback;
    }
}
