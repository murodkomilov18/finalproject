package dev.komur.servlets.user;

import dev.komur.services.basket.BasketServiceImpl;
import dev.komur.services.category.CategoryServiceImpl;
import dev.komur.services.food.FoodServiceImpl;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "UserPageServlet", value = "/user")
public class UserPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String categoryId = request.getParameter("category") == null ? "3" : request.getParameter("category");
        request.setAttribute("categories", CategoryServiceImpl.getInstance().getAll().getData());
        String userId = request.getSession().getAttribute("userId").toString();
        request.setAttribute("count", BasketServiceImpl.getInstance().getCountAllItemsInBasketByUserId(userId));
        request.setAttribute("categoryId", categoryId);
        request.setAttribute("products", FoodServiceImpl.getInstance().getFoodsByCategory(categoryId));
        request.setAttribute("user", UserServiceImpl.getInstance().get(userId).getData());
        request.setAttribute("image", UserServiceImpl.getInstance().getProfileImage(userId));
        request.getRequestDispatcher("/user/user.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
