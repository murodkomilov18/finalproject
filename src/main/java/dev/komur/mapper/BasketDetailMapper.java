package dev.komur.mapper;

import dev.komur.dtos.basket.BasketDetailCreateDTO;
import dev.komur.dtos.food.CategoryCreateDTO;
import dev.komur.entities.BasketDetail;
import dev.komur.entities.Category;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BasketDetailMapper implements BaseMapper<BasketDetail, ResultSet, BasketDetailCreateDTO>{
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static volatile BasketDetailMapper mapper;

    public static BasketDetailMapper getInstance() {
        if (mapper == null) {
            synchronized (BasketDetailMapper.class){
                if (mapper == null){
                    mapper = new BasketDetailMapper();
                }
            }
        }
        return mapper;
    }
    @Override
    public BasketDetail fromCreateDTO(@NonNull BasketDetailCreateDTO dto) {
        return BasketDetail.childBuilder()
                .foodId(dto.foodId())
                .quantity(dto.quantity())
                .createdBy(dto.createdBy())
                .basketId(dto.basketId())
                .createdAt(LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent"))))
                .build();
    }

    @Override
    public BasketDetail fromResultSet(@NonNull ResultSet resultSet) {
        try {
            return BasketDetail.childBuilder()
                    .id(String.valueOf(resultSet.getInt("id")))
                    .foodId(String.valueOf(resultSet.getInt("food_id")))
                    .quantity(resultSet.getInt("quantity"))
                    .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                    .build();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Override
    public List<BasketDetail> fromResultSetToList(@NonNull ResultSet resultSet) {
        List<BasketDetail> details = new ArrayList<>();
        try {
            while (resultSet.next()){
                details.add(BasketDetail.childBuilder()
                        .id(String.valueOf(resultSet.getInt("id")))
                        .foodId(String.valueOf(resultSet.getInt("food_id")))
                        .quantity(resultSet.getInt("quantity"))
                        .deleted(Boolean.getBoolean(resultSet.getString("is_deleted")))
                        .build());
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException(e.getLocalizedMessage());
        }
        return details;
    }
}
