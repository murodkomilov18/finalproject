package dev.komur.servlets.admin.order;

import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "ActiveOrdersServlet", value = "/active_orders")
public class ActiveOrdersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int page = 1;
        int recordsPerPage = 5;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));

        BasketService service = BasketServiceImpl.getInstance();
        List<OrderInfoDTO> infoDTOS = service.getActiveOrders(recordsPerPage, (page-1) * recordsPerPage);
        int noOfRecords = service.getNumberOfAllActiveOrders();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        request.setAttribute("infos", infoDTOS);
        request.getRequestDispatcher("/admin/orders.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
