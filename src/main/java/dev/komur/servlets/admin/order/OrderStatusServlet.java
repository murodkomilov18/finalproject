package dev.komur.servlets.admin.order;

import dev.komur.daos.UserDAO;
import dev.komur.dtos.order.OrderInfoDetailDTO;
import dev.komur.entities.User;
import dev.komur.enums.OrderStatus;
import dev.komur.enums.PaymentStatus;
import dev.komur.enums.UserStatus;
import dev.komur.services.basket.BasketService;
import dev.komur.services.basket.BasketServiceImpl;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@WebServlet(name = "OrderStatusServlet", value = "/active_orders/picked")
public class OrderStatusServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String picked = request.getParameter("picked");
        BasketService service = BasketServiceImpl.getInstance();
        if (Objects.isNull(picked)){
            String userId = request.getParameter("user_id");
            service.changeOrderStatus(id, OrderStatus.NOT_PICKED, PaymentStatus.NOT_PAID);
            User user = (User) UserServiceImpl.getInstance().get(userId).getData();
            if (user.getLoyaltyPoints() - 100 <= 0) {
                user.setLoyaltyPoints(0);
                user.setStatus(UserStatus.BLOCKED);
            }else {
                user.setLoyaltyPoints(user.getLoyaltyPoints() - 100);
            }
            UserDAO.getInstance().update(user);
        }else {
            service.changeOrderStatus(id, OrderStatus.PICKED, PaymentStatus.PAID_BY_CASH);
        }
        response.sendRedirect("/active_orders");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
