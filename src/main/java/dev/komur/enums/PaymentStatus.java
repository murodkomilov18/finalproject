package dev.komur.enums;

public enum PaymentStatus {
        NOT_PAID,
        PAID_BY_CASH,
        PAID_BY_ACCOUNT,
        WILL_BE_PAID_BY_CASH;
    }