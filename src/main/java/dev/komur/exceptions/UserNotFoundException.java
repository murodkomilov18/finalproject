package dev.komur.exceptions;

public class UserNotFoundException extends RuntimeException{
    private final Integer code;
//    public UserNotFoundException(String message) {
//        super(message);
//    }
    public UserNotFoundException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
