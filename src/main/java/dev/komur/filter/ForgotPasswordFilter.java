package dev.komur.filter;

import dev.komur.daos.UserDAO;
import dev.komur.exceptions.CustomizedException;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Objects;

@WebFilter(filterName = "ForgotPasswordFilter", urlPatterns = "/auth/forgot_password")
public class ForgotPasswordFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String email = req.getParameter("email");

        if (req.getMethod().equalsIgnoreCase("post")){
            if (Objects.isNull(email) || email.isBlank() ||
                !email.matches("^[\\w!#$%&amp;'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&amp;'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$"))
                throw new CustomizedException("Email is not valid", 400);
            if (!UserDAO.getInstance().existByEmail(email))
                throw new CustomizedException("Provided email is not registered in the system", 404);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
