package dev.komur.servlets.auth;

import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ForgotPasswordServlet", value = "/auth/forgot_password")
public class ForgotPasswordServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/auth/forgot_password.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        UserServiceImpl.getInstance().forgotPassword(email);
        PrintWriter writer = response.getWriter();
        writer.write("<div style=\"display:flex;justify-content:center;flex-direction:column;align-items:center\">" +
                "  <h3>We've sent temporary password to your email. Please check your email</h3>" +
                "  <a href=\"http://localhost:8080/auth/login\" style=\"width:100px;background-color:lime;padding:20px 30px;text-decoration:none;font-weight:bold\">Back to Login</a>" +
                "</div>");
    }
}
