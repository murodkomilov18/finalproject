# Cafe

## Description
I selected Cafe project among other projects.

## Details
Client makes Order for lunch(choose from menu) and indicates the time when he would like to receive the order.
The system shows the price order and offers to pay from the client's account or in cash upon receipt of the order.
Client Loyalty points are awarded for pre-orders. If Client makes an order and does not pick it up,
then loyalty points are removed until it is blocked. Client can evaluate every Order and leave feedback.
Administrator manages the menu, sets/removes bans/bonuses/points Clients.
