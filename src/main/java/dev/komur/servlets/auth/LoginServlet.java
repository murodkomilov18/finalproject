package dev.komur.servlets.auth;

import dev.komur.dtos.user.LoginRequest;
import dev.komur.entities.User;
import dev.komur.services.user.UserService;
import dev.komur.services.user.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

import java.io.IOException;
import java.util.Objects;

@WebServlet(name = "LoginServlet", value = "/auth/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("next", request.getParameter("next"));
        Cookie[] cookies = request.getCookies();
        String value = null;
        for (Cookie cookie : cookies) {
            if (Objects.equals(cookie.getName(),"auth")){
                value = cookie.getValue();
            }
        }
        if (Objects.isNull(value)) {
            request.getRequestDispatcher("/auth/login.jsp").forward(request, response);
        } else {
            if (value.split("/")[0].equals("ADMINISTRATOR")) {
                response.sendRedirect("/admin");
            }else {
                response.sendRedirect("/user");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserService userService = UserServiceImpl.getInstance();
        LoginRequest loginDto = (LoginRequest) request.getAttribute("loginDto");
        User user = userService.checkLogin(loginDto);
        Cookie cookie = userService.createCookie(user.getId(), user.getRole().name());
        response.addCookie(cookie);
        if (Objects.equals(user.getRole().name(), "ADMINISTRATOR")){
            response.sendRedirect("/admin");
        }else {
            response.sendRedirect("/user");
        }
    }
}
