package dev.komur.dtos.user;

public record LoginRequest(String email, String password) {
}
