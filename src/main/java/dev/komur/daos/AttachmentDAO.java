package dev.komur.daos;

import dev.komur.entities.Attachment;
import dev.komur.entities.Food;
import dev.komur.mapper.AttachmentMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AttachmentDAO extends DAO<Attachment, String> {
    private static volatile AttachmentDAO instance;
    private final AttachmentMapper mapper = AttachmentMapper.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());

    private static final String INSERT_ATTACHMENT_QUERY = """
            insert into attachment(file_original_name, size, main_content, linked_id, linked_name, created_by, created_at) values(?, ?, ?, ?, ?, ?, ?) returning id;
            """;

    private static final String UPDATE_ATTACHMENT_QUERY = """
            update attachment set file_original_name = ?, size = ?, main_content = ?, linked_id = ?, linked_name = ?, updated_at = ?, updated_by = ? where id = ?;
            """;

    public static AttachmentDAO getInstance() {
        if (instance == null) {
            synchronized (AttachmentDAO.class){
                if (instance == null){
                    instance = new AttachmentDAO();
                }
            }
        }
        return instance;
    }

    @Override
    public Attachment save(Attachment attachment) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_ATTACHMENT_QUERY)) {
            pr.setString(1, attachment.getFileOriginalName());
            pr.setLong(2, attachment.getSize());
            pr.setBytes(3, attachment.getMainContent());
            pr.setString(4, attachment.getLinkedId());
            pr.setString(5, attachment.getLinkedName());
            pr.setString(6, attachment.getCreatedBy());
            pr.setDate(7, Date.valueOf(attachment.getCreatedAt().toLocalDate()));

            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                attachment.setId(String.valueOf(resultSet.getInt("id")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving the attachment in database");
        }
        return attachment;
    }

    @Override
    public Optional<Attachment> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from attachment where id = ?;")) {
            pr.setInt(1 , Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Attachment attachment = mapper.fromResultSet(resultSet);
                return Optional.of(attachment);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the attachment from database");
        }
        return Optional.empty();
    }

    @Override
    public List<Attachment> getAll() {
        Connection connection = getConnection();
        List<Attachment> attachments;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from attachment;");
            attachments = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all attachment from database");
        }
        return attachments;
    }

    @Override
    public int update(Attachment attachment) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_ATTACHMENT_QUERY)){
            pr.setString(1, attachment.getFileOriginalName());
            pr.setLong(2, attachment.getSize());
            pr.setBytes(3, attachment.getMainContent());
            pr.setString(4, attachment.getLinkedId());
            pr.setString(5, attachment.getLinkedName());
            pr.setDate(6, Date.valueOf(LocalDate.now()));
            pr.setString(7, attachment.getUpdatedBy());
            pr.setInt(8, Integer.parseInt(attachment.getId()));
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the attachment");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update attachment set is_deleted = true where id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            return pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the attachment");
        }
    }

    public Optional<Attachment> getByLinkedId(String linkedId, String linkedName){
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from attachment where linked_id = ? and linked_name = ?;")) {
            pr.setString(1, linkedId);
            pr.setString(2, linkedName);
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Attachment attachment = mapper.fromResultSet(resultSet);
                return Optional.of(attachment);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the attachment from database");
        }
        return Optional.empty();
    }
}
