package dev.komur.entities;

import lombok.*;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BasketDetail extends BaseEntity{
    private String foodId;
    private Integer quantity;
    private String basketId;

    @Builder(builderMethodName = "childBuilder")
    public BasketDetail(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, @NonNull String foodId, Integer quantity, String basketId) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.foodId = foodId;
        this.quantity = quantity;
        this.basketId = basketId;
    }
}
