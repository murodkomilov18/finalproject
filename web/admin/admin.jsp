<%--
  Created by IntelliJ IDEA.
  User: Hp
  Date: 04-07-2023
  Time: 10:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin page</title>
    <link rel="stylesheet" href="../resources/css/admin_dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
          crossorigin="anonymous">
</head>
<body>
<section>
    <div class="sidebar">
        <h3 class="logo">Admin <br>Dashboard</h3>
        <ul class="admin_navbar">
            <li class="active"><a href="/admin"><i class="fa fa-windows"></i> Dashboard</a></li>
            <li><a href="/active_orders"><i class="fa fa-shopping-bag"></i> Orders</a></li>
            <li><a href="/food"><i class="fa fa-pie-chart"></i> Menu</a></li>
            <li><a href="/category"><i class="fa fa-cube"></i> Category</a></li>
            <li><a href="/feedbacks"><i class="fa fa-comments"></i> Users Feedback</a></li>
            <li><a href="/auth/change_password"><i class="fa fa-cog"></i> Change Password</a></li>
            <li><a href="/auth/logout"><i class="fa fa-power-off"></i> Log out</a></li>
        </ul>
    </div>
    <div class="main">
        <div class="head-section">
            <div class="column-6">
                <h2>Clients</h2>
            </div>
        </div>
        <div class="content">
            <table class="table table-striped table-bordered text-center">
                <thead class="table-dark">
                <tr>
                    <th>№</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${i = i + 1}</td>
                        <td>${user.getFirstName()} ${user.getLastName()}</td>
                        <td>${user.getEmail()}</td>
                        <td>${user.getStatus()}</td>
                        <td>
                            <c:choose>
                                <c:when test="${user.getStatus() == 'ACTIVE'}">
                                    <a href="/user/unblock?id=${user.getId()}" class="btn btn-primary disabled">Unblock</a>
                                    <a href="/user/ban?id=${user.getId()}" class="btn btn-danger">Ban</a>
                                </c:when>
                                <c:otherwise>
                                    <a href="/user/unblock?id=${user.getId()}" class="btn btn-primary">Unblock</a>
                                    <a href="/user/ban?id=${user.getId()}" class="btn btn-danger disabled">Ban</a>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</section>
</body>
</html>
