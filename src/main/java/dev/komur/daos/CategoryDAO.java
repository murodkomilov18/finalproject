package dev.komur.daos;

import dev.komur.entities.Category;
import dev.komur.entities.Food;
import dev.komur.mapper.CategoryMapper;
import dev.komur.mapper.FoodMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CategoryDAO extends DAO<Category, String> {
    private static volatile CategoryDAO instance;
    private final CategoryMapper mapper = CategoryMapper.getInstance();
    private final Logger logger = Logger.getLogger(getClass().getName());
    private static final String INSERT_FOOD_QUERY = """
            insert into category(name, created_at, created_by) values(?, ?, ?) returning id;
            """;

    private static final String UPDATE_FOOD_QUERY = """
            update category set name= ?, updated_at = ?, updated_by = ? where id = ?;
            """;

    public static CategoryDAO getInstance() {
        if (instance == null) {
            synchronized (CategoryDAO.class){
                if (instance == null){
                    instance = new CategoryDAO();
                }
            }
        }
        return instance;
    }
    @Override
    public Category save(Category category) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(INSERT_FOOD_QUERY)) {
            pr.setString(1, category.getName());
            pr.setDate(2, Date.valueOf(category.getCreatedAt().toLocalDate()));
            pr.setString(3, category.getCreatedBy());

            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                category.setId(String.valueOf(resultSet.getInt("id")));
            }
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while saving the category in database");
        }
        return category;
    }

    @Override
    public Optional<Category> getById(String id) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from category where id = ?;")) {
            pr.setInt(1 , Integer.parseInt(id));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Category category = mapper.fromResultSet(resultSet);
                return Optional.of(category);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the category from database");
        }
        return Optional.empty();
    }

    @Override
    public List<Category> getAll() {
        Connection connection = getConnection();
        List<Category> categories;
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from category where is_deleted = false order by name;");
            categories = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting all categories from database");
        }
        return categories;
    }

    @Override
    public int update(Category category) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement(UPDATE_FOOD_QUERY)){
            pr.setString(1, category.getName());
            pr.setDate(2, Date.valueOf(LocalDate.now()));
            pr.setString(3, category.getUpdatedBy());
            pr.setInt(4, Integer.parseInt(category.getId()));
            return pr.executeUpdate();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while updating the category");
        }
    }

    @Override
    public boolean delete(String id) {
        Connection connection = getConnection();
        try (PreparedStatement pr = connection.prepareStatement("update category set is_deleted = true where id = ?;")){
            pr.setInt(1, Integer.parseInt(id));
            return pr.execute();
        }catch (SQLException e){
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while deleting the category");
        }
    }

    public boolean existsByName(String name) {
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select * from category where name = ?;")) {
            pr.setString(1 , name);
            ResultSet resultSet = pr.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the category from database");
        }
    }

    public Optional<Category> getFoodCategory(String foodId){
        Connection connection = getConnection();
        try(PreparedStatement pr = connection.prepareStatement("select c.id, c.name, c.is_deleted from category c where c.id = (select f.category_id from food f where f.id = ?);")) {
            pr.setInt(1 , Integer.parseInt(foodId));
            ResultSet resultSet = pr.executeQuery();
            if (resultSet.next()){
                Category category = mapper.fromResultSet(resultSet);
                return Optional.of(category);
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting the category from database");
        }
        return Optional.empty();
    }

    public List<Category> getByPage(int recordsPerPage, int offset) {
        Connection connection = getConnection();
        List<Category> items;
        try(PreparedStatement pr = connection.prepareStatement("select * from category where is_deleted = false order by name limit ? offset ?;")) {
            pr.setInt(1, recordsPerPage);
            pr.setInt(2, offset);
            ResultSet resultSet = pr.executeQuery();
            items = mapper.fromResultSetToList(resultSet);
        } catch (SQLException e) {
            logger.info(e.getMessage());
            throw new RuntimeException("Something went wrong while getting categories by page from database");
        }
        return items;
    }
}
