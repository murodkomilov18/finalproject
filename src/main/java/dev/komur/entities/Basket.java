package dev.komur.entities;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Basket extends BaseEntity {
    private String userId;
    private List<BasketDetail> details;

    @Builder(builderMethodName = "childBuilder")
    public Basket(String id, boolean deleted, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, @NonNull String userId, List<BasketDetail> details) {
        super(id, deleted, createdAt, updatedAt, createdBy, updatedBy);
        this.userId = userId;
        this.details = details;
    }
}
