package dev.komur.services.user;

import dev.komur.dtos.user.ChangePasswordRequest;
import dev.komur.dtos.user.LoginRequest;
import dev.komur.dtos.user.UserCreateDTO;
import dev.komur.dtos.user.UserUpdateDTO;
import dev.komur.entities.User;
import dev.komur.enums.UserStatus;
import dev.komur.services.base.GenericCrudService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.Part;


public interface UserService extends GenericCrudService<UserCreateDTO, UserUpdateDTO, String> {
    void confirmAccount(String userId, String code);
    Cookie createCookie(String userId, String role);

    User checkLogin(LoginRequest loginRequest);

    void forgotPassword(String email);

    void changePassword(ChangePasswordRequest dto, String userId);

    void setProfileImage(String userId, Part filePart);

    String getProfileImage(String userId);

    void changeUserStatus(String userId, UserStatus banned);

}
