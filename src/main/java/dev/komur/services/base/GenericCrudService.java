package dev.komur.services.base;

import dev.komur.dtos.Dto;
import dev.komur.dtos.GenericDto;
import dev.komur.dtos.response.Response;
import lombok.NonNull;

import java.io.Serializable;


/**
 *
 * @param <CD> Create DTO for creating domain in db
 * @param <UD> Update DTO for updating specific domain
 * @param <ID> Id
 */
public interface GenericCrudService<
        CD extends Dto,
        UD extends GenericDto,
        ID extends Serializable> {
    Response create(@NonNull CD dto);

    Response update(@NonNull UD dto);

    Response delete(@NonNull ID id);

    Response get(@NonNull ID id);

    Response getAll();
}
