package dev.komur.servlets.admin.order;

import dev.komur.dtos.order.OrderInfoDTO;
import dev.komur.services.basket.BasketServiceImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "FeedbacksServlet", value = "/feedbacks")
public class FeedbacksServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<OrderInfoDTO> details = BasketServiceImpl.getInstance().getUserFeedbacks();
        request.setAttribute("details", details);
        request.getRequestDispatcher("/admin/feedbacks.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
