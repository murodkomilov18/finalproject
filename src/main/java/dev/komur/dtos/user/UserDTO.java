package dev.komur.dtos.user;

import dev.komur.dtos.GenericDto;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDTO extends GenericDto {
    private String firstName;
    private String lastName;
    private String password;
    private String status;
    private String email;
    private String role;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String createdBy;
    private String updatedBy;

    @Builder(builderMethodName = "childBuilder")
    public UserDTO(String id, String firstName, String lastName, String password, String status, String email, String role, LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.role = role;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }
}
