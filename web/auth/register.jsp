<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Sign up Form </title>
        <link rel="stylesheet" href="../resources/css/style.css">
                        
    </head>
    <body>
        <section class="container forms">
            <div class="form signup">
                <div class="form-content">
                    <header>Sign up</header>
                    <form action="${pageContext.request.contextPath}/auth/register" method="post">
                        <div class="field input-field">
                            <input type="text" placeholder="Firstname" class="input" name="firstname">
                        </div>
                        <div class="field input-field">
                            <input type="text" placeholder="Lastname (optional)" class="input" name="lastname">
                        </div>
                        <div class="field input-field">
                            <input type="email" placeholder="Email" class="input" name="email">
                        </div>

                        <div class="field input-field">
                            <input type="password" placeholder="Create password" class="password" name="password">
                        </div>

                        <div class="field input-field">
                            <input type="password" placeholder="Confirm password" class="password" name="confirmPassword">
                            <i class='bx bx-hide eye-icon'></i>
                        </div>

                        <div class="field button-field">
                            <button>Signup</button>
                        </div>
                    </form>

                    <div class="form-link">
                        <span>Already have an account? <a href="${pageContext.request.contextPath}/auth/login" class="link login-link">Sign in</a></span>
                    </div>
                </div>
            </div>
        </section>

        <script src="../resources/script/auth.js"></script>
    </body>
</html>